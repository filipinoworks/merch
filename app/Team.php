<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\TeamMember;

class Team extends Model
{
    //
    
	public function getTeamMembers()
	{
		$user_id = JWTAuth::user()->id;
		$team_ins = Team::where("creator", $user_id)->first();

		$team_members = TeamMember::where("team_id", $team_ins->id)
						->with("member")
						->with("member.userProfile")
						->with("team.owner")
						->get();

		foreach ($team_members as $key => $team_member) {
            $member = User::find($team_member->member->id);
			$team_member->role = $member->roles->pluck('name');
		}

		return $team_members;
	}

	public function owner() 
    {
        return $this->belongsTo('App\User', 'creator');
    }

    public function team_members() 
    {
        return $this->hasMany('App\TeamMember', 'team_id');
    }

    public function createTeam($user)
    {
    	$current_user_id = JWTAuth::user()->id;
    	$team = Team::where("creator" , $current_user_id)->first();
    
    	if($team == null)
    	{	
    		$team = new Team();
    		$team->creator = $current_user_id;
    		$team->save();
    	}

    	
    	$this->addToTeam($user, $team->id);
    	
    }

    public function addToTeam($user, $team_id)
    {
    	$check_if_member = TeamMember::where("user_id", $user["id"])->where("team_id", $team_id)->get();

    	if(count($check_if_member) == 0) 
    	{
    		$team_member_ins = new TeamMember();
    		$team_member_ins->team_id = $team_id;
    		$team_member_ins->user_id = $user["id"];
    		$team_member_ins->save();
    	}
    }
}
