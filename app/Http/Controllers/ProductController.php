<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use JWTAuth;
use DB;
class ProductController extends Controller
{
    //
    public function store(Request $rq)
    {
    	$product_ins = new Product();
    	$product = $product_ins->storeProduct($rq->all());
    	
    	return response()->json("Product has been created.", 200);
    }
    public function show(Product $product, $id)
    {
    	return $product::find($id);
    }
    public function index(Request $rq)
    {
        if(isset($rq["not_in_sales_channel"])) 
        {
            $products = DB::table("products")->select('*')->where("user_id" , JWTAuth::user()->id)->whereNotIn('id',function($query) use ($rq) {

               $query->select('product_id')->where("sales_channel_id", $rq->sales_channel_id)->from('sales_channel_products');

            })->get();
        }else
        {
            $products =  Product::where("user_id" , JWTAuth::user()->id)->get();
        }
    	
    	
    	return $products;
    }
    public function delete(Product $product , $id)
    {
    	$product =  $product->findOrFail($id);
    	$product->delete();

    	return response()->json("Product has been removed.", 200);
    }
}
