<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\ListingProduct;
use App\ListingPhotoPath;
use JWTAuth;
use Storage;
use File;
use Image;

class ListingController extends Controller
{
    //

    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getPhoto($id)
    {
    	$photo = ListingPhotoPath::find($id);
    	$photo_ins = new ListingPhotoPath();
		$has_access = $photo_ins->hasAccess($id);    	
    	
		if($has_access) 
		{
			$path = base_path("storage/app/designs/uploads/").$photo->path_code."/thumbs/".$photo->filename;
			$img = Image::make(file_get_contents($path));
			return $img->response('png', 70);
		}
		return response("Access forbidden.", 403);
    }

    public function photos($id)
    {
    	$photo = Listing::find($id);
    	$photo_ins = new ListingPhotoPath();
		$photos = $photo->getPhotos($photo->path_code);

		return $photos;
    }

    public function getStatusListing($listing_id) 
    {
    	
    	$status = "Design Needed";
    	$photos = $this->photos($listing_id);

    	//check if content needed
    	$listing = Listing::find($listing_id);

    	if(empty($photos)) 
    	{
    		return $status;
    	}

    	//if there is listing
    	if(!empty($listing)) 
    	{	
    		if(empty($listing->brand) || empty($listing->title) || empty($listing->keywords)) 
    		{
    			return "Content Needed";
    		}

    		if(!empty($listing->brand) && empty($listing->title) && empty($listing->keywords)) 
    		{
    			return "Ready to upload";
    		}
    		
    	}

    	return $status;
    }

	public function uploadPhotos(Request $rq)
	{

		$user = JWTAuth::user();

		if($user == null) 
		{
			return response("Sorry, please login to upload your designs.", 403);
		}

		if($rq->file != null){

	        $file = $rq->file;      
	      
	        $filename = $file->getClientOriginalName();      
	        $dimensions = getimagesize($rq->file);
			$width = $dimensions[0];
			$height = $dimensions[1];

	        $data_obj = $rq->all();
	        $count = 1;        
	        
	        $listing_id = "";

	        if(isset($data_obj["listing_id"])) 
	        {

	        	$listing_id = $data_obj["listing_id"];
	        	$listing_ins = Listing::find($data_obj["listing_id"]);

	        	
	        	if($listing_ins["path_code"] != null)
	        	{
	        		$path_code = $listing_ins["path_code"];
	        	}else{
	        		$path_code = $data_obj["path_code"];
	        		$listing_ins->path_code = $path_code;
	        		$listing_ins->save();
	        	}

	        }else{
	        	$path_code = $data_obj["path_code"];
	        }


	        while(Storage::disk('designs')->has("uploads/$path_code/$filename")) {
	            $filename = $count."-".$filename;
	            $count++;
	        }

	        $design_path = "uploads/$path_code/$filename";
	        $root_path = base_path('storage/app/designs/');
			$thumb_path = "uploads/$path_code/thumbs/$filename";


	        Storage::disk("designs")->put($design_path,file_get_contents($rq->file));
	        Storage::disk("designs")->put($thumb_path,file_get_contents($rq->file));

	        $image = Image::make($root_path.$design_path)->resize(300, 200);
	        
	        $image->save($root_path.$thumb_path, 60);

	        
	        $photo_id = $this->savePhotoDetails($path_code, $filename, $width, $height, $design_path);

	        return response()->json(array("path_code" => $path_code, "id" => $photo_id) , 200); 
	      }  
	}

	public function savePhotoDetails($path_code, $filename, $width, $height, $path)
	{

		//check if there is a similar file
		$listing_photo_check = ListingPhotoPath::where("path_code", $path_code)
												->where("filename", $filename)
												->where("width", $width)
												->where("height", $height)->first();

		if(!empty($listing_photo_check)) 
		{
			return $listing_photo_check->id;
		}

		$listing_photo_paths = new ListingPhotoPath();
		$listing_photo_paths->path_code = $path_code;
		$listing_photo_paths->filename = $filename;
		$listing_photo_paths->width = $width;
		$listing_photo_paths->height = $height;
		$listing_photo_paths->path = $path;
		$listing_photo_paths->save();

		return $listing_photo_paths->id;

	}

    public function store(Request $rq)
    {
    	$listing_ins = new Listing();
    	$listing_ins = $listing_ins->storeListing($rq->all());
    	
    	return response("Listing has been saved.", 200);
    }

    public function delete($id)
    {
    	$listing_ins = Listing::find($id);
    	if($listing_ins)
    	{
    		if($listing_ins->user_id == JWTAuth::user()->id)
    		{
    			$listing_ins->delete();	
    		}
    	}

    	return response("Listing has been removed.", 200);
    }

    public function removePhoto($id)
    {
    	$listing_photo_ins = ListingPhotoPath::where("id", $id)->with('listing','research')->first();
    	if($listing_photo_ins)
    	{	
    		if($listing_photo_ins->hasAccess($id))
    		{
    			$listing_photo_ins->delete();	
    		}
    	}

    	return response("Photo has been removed.", 200);
    }

    public function index()
    {
    	$listing_ins = new Listing();

    	return $listing_ins->getListing();
    }
}
