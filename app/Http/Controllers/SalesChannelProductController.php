<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesChannelProduct;

class SalesChannelProductController extends Controller
{
    //
    public function show(SalesChannelProduct $sales_channel_product, $id)
    {
        $sales_channel_product = $sales_channel_product::find($id);

    	return $sales_channel_product;
    }
	public function index(Request $rq)
    {
    	$sales_channel_product = new SalesChannelProduct();
        $sales_channel_product = $sales_channel_product->getSalesProducts($rq->all());

    	return $sales_channel_product;
    }
    public function store(Request $rq, SalesChannelProduct $sales_channel_product)
    {
    	$sales_channel_product->saveProduct($rq->all());

    	return response("Changes has been saved.", 200); 
    }

    public function delete(SalesChannelProduct $sales_channel_product, $id)
    {
    	$sales_channel_product =  $sales_channel_product->findOrFail($id);
    	$sales_channel_product->delete();

    	return response()->json("Product has been saved.", 200); 
    }


}
