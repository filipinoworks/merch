<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\OldPosts;
use App\UsersProfile;
use App\OldUsers;
use App\User;
use App\JobStatusHistory;
use App\EthesiaEvent;
use Hash;
use DB;
use Auth;
use Carbon\Carbon;
use App\SubuserJobpostPermissions;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    //

    public function prepareRoles()
    {

        if(count(Role::all()) == 0) {
 
            $role = Role::create(["name" => "Owner"]);
            $role = Role::create(["name" => "Manager"]);
            $role = Role::create(["name" => "Team Member"]);
            $role = Role::create(["name" => "Designer"]);
            return  "Roles has been created";
        }

        return  "Sorry Roles has already been created.";
        
    }

    public function impersonate(Request $rq, SubuserJobpostPermissions $subserPermissions)
    {

        $id = $rq->id;
        $user = User::find($id);
        
        // Guard against administrator impersonate
        if(Auth::user()->userAdmin == 1)
        {
            Auth::user()->impersonate($user);
            $data = array();
            $data["status"] = true;
            $data["message"] = "Welcome.";
            $data["credentials"] = Auth::user();
            $data["credentials"]["impersonate"] = true;
            $data["profile"] = UsersProfile::where("userID", Auth::user()->id)->first();
            $data['subuser'] = $subserPermissions->where('user_id',Auth::user()->id)->first();
            return $data;
        }
        else
        {
            $data = array();
            $data["status"] = false;
            return $data;
        }
    }

    public function getJobProviders($user_id = null)
    {   
        $user_ins = new User();
        return $user_ins->getJobProvider($user_id);
    }

    public function countJobPostChart(Request $rq)
    {
        if($rq->company != "all" && $rq->company != "")
        {
            $job_providers = $this->getJobProviders($rq->company);
        }else
        {
            $job_providers = $this->getJobProviders();
        }


        $data_sets = array();
        foreach ($job_providers as $key => $job_provider) {
            # code...
            $data_sets[$key] = array();
            $color = substr(md5(rand()), 0, 6);
            
            if($job_provider->userProfile != null)
            {
                $data_sets[$key]["label"] = $job_provider->userProfile->company_name; 
            }
            else
            {
                continue;
            }
            
            $data_sets[$key]["fillColor"] = "#$color";
            $data_sets[$key]["strokeColor"] = "#$color";
            $data_sets[$key]["pointColor"] = "#$color";
            $data_sets[$key]["pointStrokeColor"] = "#$color";
            $data_sets[$key]["pointHighlightFill"] = "#$color";
            $data_sets[$key]["pointHighlightStroke"] = "#$color";
            $counting = $this->countJobPosts($job_provider->id, $rq->post_status);
            $data_sets[$key]["data"] = $counting["sets"];
        }

        return $data_sets;

        
    }


    public function countJobPosts($user_id = null, $post_status = null) 
    {

        $data = new JobStatusHistory();
        $data = $data->groupBy("job_id")->get();

        if($user_id != null) 
        {
            $data = $data->where("user_id", $user_id);
        }   
        
        if($post_status != null)
        {
            $data = $data->where("post_status", $post_status);
        }

        $data = $data->groupBy(function($val) {
                    return Carbon::parse($val->created_at)->format('m');
                });

        $months = range(0,11);
        $sets = array();
    
        foreach ($months as $key => $month) {
            $sets[$key] = isset($data[$month  + 1]) ? count($data[$month  + 1]) : 0;
        }

        $response = array();
        $response["status"] = true;
        $response["sets"] = $sets;
        return $response;
    }


    public function timelines()
    {
        $ethesia_event = EthesiaEvent::limit(30)->orderBy('id', 'desc')->get();

        foreach ($ethesia_event as $key => $event) {

            if($event->user_id != "") 
            {
                $user = User::find($event->user_id);
                $event->name = $user->getName();
                $event->time_ago = $user->getTimeAgo($event->created_at);
                $event->image = $user->getImage();
            }else
            {
                $user_ins = new User();
                $event->time_ago = $user_ins->getTimeAgo($event->created_at);
                $event->image = $user_ins->getImageEvent($event->name);
            }
            
        }

        return $ethesia_event;
    }


    public function getUsers() 
    {
        $current = Auth::user();
        
        if($current->userAdmin == 0) {
            flash()->error('Impersonate disabled for this user.');
            return;
        } 

        $user = new User();
        $users = $user->getAllUsers();
        return $users;
    }

    public function stopImpersonate()
    {
        Auth::user()->leaveImpersonation();
        $data = array();
        $data["status"] = true;
        return $data;
    }
    public function syncUpdatedAt()
    {
    	$jobs = Jobs::get();
    	foreach ($jobs as $key => $job) {
    		# code...
    		$current_ins =  Jobs::find($job->id);
			$current_ins->updated_at = $job->created_at;
			$current_ins->save();
			$current_ins->touch();	
    	}
    }

    public function transferFields()
    {

    	return;

    	$old = OldUsers::find(230);

    	$data_state = $this->selectRandomState();

    	$new_user = new User();
		$new_user->email = $old->user_email;
		$new_user->password = Hash::make("looping123");
		$new_user->userType = 2;
		$new_user->userTitle = 1;
		$new_user->save();

		$new_user_id = $new_user->id;

		$new_profile = new UsersProfile();
		$new_profile->userID = $new_user_id;
		$new_profile->company_name = $old->display_name;
		$new_profile->image_path = "";
		$new_profile->city = $data_state["city"];
		$new_profile->state = $data_state["state"];
		$new_profile->description = $old->display_name." is one of the leading providers of healthcare solutions for anesthesiology and other specialties to physicians, hospitals and outpatient centers. Physician led and managed,.  provides comprehensive hospital-based clinical and management solutions for anesthesia outsourcing and other specialty areas including emergency medicine, women's and children's.";
		$new_profile->save();
		


		$old_posts = new OldPosts();
    	$old_posts = $old_posts->where("post_author",230)->limit(30)->get();


    	foreach ($old_posts as $key => $old_post) {
    		
    		$data_state = $this->selectRandomState();

			$new_post                     = new Jobs();
			$new_post->userID             = $new_user_id;
			$new_post->jobTitle           = $old_post->post_title;
			$new_post->ei_jobDescription  = $old_post->post_content;
			$new_post->created_at         = $old_post->post_date_gmt;
			$new_post->post_status        = "active";
			$new_post->ei_durPosition     = "Full Time"; 
			$new_post->jobPostType        = 1; 
			$new_post->ei_facilityAddress = ""; 
			$new_post->ei_facilityCity    = $data_state["city"]; 
			$new_post->ei_facilityState   = $data_state["state"]; 
			$new_post->ei_startImmediate  = true; 
			$new_post->save();
		}

    	return "transfered";
    }

    public function selectRandomState() 
    {
    	$states = array("Florida" => "Alford" ,
    					"Florida" => "Appolo Beach",
    					"Florida" => "Baldwin",
    					"California" => "San Diego",
    					"California" => "Lake San Marcos",
    					"California" => "Alturas",
    					"California" => "San Diego",
    					"California" => "Lake San Marcos",
    					"California" => "Carmichael",
    					"Illinois" => "Addison",
    					"Illinois" => "Albany",
    					"Illinois" => "Adeline"
    					);


    	$state = array_rand($states);

    	$data = array();
    	$data["state"] = $state;
    	$data["city"] = $states[$state];
    	return $data;
    		 
    }

    public function transferUsers()
    {
    	$old_users = new OldUsers();
    	$old_users = $old_users->limit(100)->get();

    	foreach ($old_users as $key => $old) {
			$new_user                     = new User();
			$new_user->email = $old->user_email;
			$new_user->password = Hash::make("looping123");
			$new_user->userType = 2;
			$new_user->userTitle = 1;
			$new_user->save();

			$new_profile = new UsersProfile();
			$new_profile->userID = $new_user->id;
			$new_profile->company_name = $old->display_name;
			$new_profile->image_path = "";
			$new_profile->city = "San Diego";
			$new_profile->state = "Ca";
			$new_profile->description = $old->display_name." is one of the leading providers of healthcare solutions for anesthesiology and other specialties to physicians, hospitals and outpatient centers. Physician led and managed,.  provides comprehensive hospital-based clinical and management solutions for anesthesia outsourcing and other specialty areas including emergency medicine, women's and children's.";
			$new_profile->save();
		}

    	return "transfered";
    }
}
