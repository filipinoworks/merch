<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\TeamMember;

class TeamController extends Controller
{
    //

    public function getUserById($user_id)
    {
    	$team_user_ins = new TeamMember();
    	return $team_user_ins->getDetails($user_id);
    }

    public function store(Request $rq)
    {
    	$user_ins = new User();
        $user = $user_ins->makeUser($rq->all(), true);

        $team_ins = new Team();
        $team_ins->createTeam($user["credentials"]);

        $user["message"] = "Team member has been saved.";
        return $user;
    }

    public function index()
    {
    	$team = new Team();
    	$team_members = $team->getTeamMembers();

    	return $team_members;
    }
}
