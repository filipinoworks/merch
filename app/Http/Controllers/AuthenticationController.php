<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\User;
use App\UsersProfile;
use App\SubuserJobpostPermissions;

class AuthenticationController extends Controller
{
    //                
    public function forgot_password_page()
    {
        if($this->redirectIfLogin()){
            return redirect('/application');
        }
        return view('authentication.forgot-password');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        $user_ins = User::where("email", $user->email)->first();

        if($user_ins) 
        {
            auth()->login($user_ins, true);

            $data = array();
            $data["status"] = true;
            $data["message"] = "Welcome.";
            $data["credentials"] = Auth::user();
            $data["profile"] = UsersProfile::where("userID", Auth::user()->id)->first();
            $data['subuser'] = SubuserJobpostPermissions::where('user_id',Auth::user()->id)->first();
       
            return view("authentication.preparing_auth_data")->with("data", $data);
        }
        else
        {
            //not regstered
            return redirect("/authentication/#/signup/?email=$user->email&social=google");

        }
       
    }

    public function login_page() 
    {                
        if($this->redirectIfLogin()){
            return redirect('/application');
        }
        return view("authentication.login");
    }

    public function signup_page() 
    {
        if($this->redirectIfLogin()){
            return redirect('/application');
        }
        return view("authentication.signup");
    }

    private function redirectIfLogin(){
        if(Auth::check()){                        
            return true;
        }
        return false;        
    }
}
