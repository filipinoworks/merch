<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\UsersProfile;
use App\User;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

trait CustomAuth
{
    //use RedirectsUsers, ThrottlesLogins;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */


    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        
        //$this->validateLogin($request);
        $remember = $request->remember_user == true ? true : false;

        $credentials = request(['email', 'password']);
        $token = "";

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Invalid username or password'], 401);
        }

        $data = array();
        $data["status"] = true;
        $data["message"] = "Welcome.";
        $data["token"] = $token;


        $jwt_user = JWTAuth::user();
        $user_ins = User::find($jwt_user->id);
        if(count($user_ins->roles) > 0) 
        {
            $data["role"] = $user_ins->roles->pluck('name')[0];
        }

        $data["credentials"] = Auth::user();
        $data["profile"] = UsersProfile::where("userID", Auth::user()->id)->first();
        $data["test"] = $user_ins->roles;
        $description = "Is now online.";
        return $data;

    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        /*return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );*/
        return  Auth::attempt(['email' => $request->email, 'password' => $request->password]);
    }

    protected function isConfirmed(Request $request)
    {
        $request->is_confirmed = "yes";
        
        return  Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_confirmed' => $request->is_confirmed]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $request->is_confirmed = 'yes';

        return $request->only($this->username(), 'password', 'is_confirmed');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        
        $this->guard()->logout();
        $data = array();
        $data["status"] = true;
        $data["message"] = "Is now offline.";

        return $data;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
