<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App;
use App\User;
use App\UsersProfile;
use Cartalyst\Stripe\Stripe;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Storage;
use Hash;
use Auth;
use Image;
use File;
use Mail;
use Avatar;
use JWTAuth;
class UsersController extends Controller
{
    //


    public function __construct()
    {
      
    }    

    public function register(Request $rq)
    {
        $user_ins = new User();
        $user = $user_ins->makeUser($rq->all());
        return $user;
    }

    public function logmeout()
    {
        Auth::logout();
    }
    public function deleteSecEmail(Request $rq, SecondaryEmail $mails)
    {

      $mails->find($rq->mailid)->delete();      

    }

    public function proceedupdatesecemail(Request $rq,SecondaryEmail $mails)
    {

      $rq->validate([
        'email' => 'required|email',
      ]);

      $mails->find($rq->mailid)->update(array(
        'secondary_email' => $rq->email,
      ));
    }

    public function getSecondarymail(SecondaryEmail $mails)
    {
      return $mails->where('user_id',Auth::User()->id)->get();
    }

    public function secondarymail(Request $rq, SecondaryEmail $mails)
    {
      $rq->validate([
        'email' => 'required|email',
      ]);

      $mails->create(array(
        'user_id' => Auth::User()->id,
        'secondary_email' => $rq->email,
      ));

    }

    public function resendAccoutVerification()
    {

      $user = Auth::User();

      $verification_code = $this->generateRandomString(10);

      $sendto = array(
        'name'=> $user->account_type == 2 ? $user->company_name : $user->first_name. " ".$user->last_name,
        'to' => $user->email,
        'verification_code' => $verification_code,
        'link' => url("/")."/verify/".$verification_code."/".$user->id,
      );

      VerificationUser::where('user_id', Auth::User()->id)->delete();      

      $user_verify = new VerificationUser();    
      $user_verify->verification_code = $verification_code;
      $user_verify->user_id = Auth::User()->id;
      $user_verify->save();

      Mail::send(new SendNewUserMail($sendto));
    }

    public function credentials()
    {
      return Auth::User()->is_confirmed;
    }

    public function verifyEmail(Request $rq, VerificationUser $verificationUser, User $user)
    {

      $verify = $verificationUser->where('user_id',$rq->user_id)->where('verification_code',$rq->code)->get();            

      if(count($verify) >= 1)
      {

        $user->where('id',$rq->user_id)->update(array(
          
          'is_confirmed' => 'yes',
        
        ));        

        return redirect(url('/application/#/dashboard'));

      }

    }

    public function testMail() 
    {

      $data = array();
      $data["subject"] = "Some Subject right hersssse";
      $data["email_from"] = "hello@anesthesia.community";
      $data["name"] = "Ralph Degoma";
      $data["to"] = "personal.ralphdegoma@gmail.com";

      SendConfirmEmail::dispatch($data);
    }

    public function unlockLockscreen(Request $request)
    {
      $hashed_password = Auth::User()->password;
      $data = array();

      if(Hash::check($request->password, $hashed_password)) 
      {
        broadcast(new OnlineStatus("online", Auth::User()->id))->toOthers();
        $data["status"] = true;
        return $data;
      }

      $data["status"] = false;


      return $data;
    }

    public function get_credentials()
    {

      $data = array();
      if(Auth::check()) 
      {
        $user_ins = new User();
        $data["credentials"] = Auth::user();
        $data["credentials"]["impersonate"] = $manager->isImpersonating();
        $data["profile"] = UsersProfile::where("userID", Auth::user()->id)->first();
      }

      return $data;

    }

    public function checkLogin(Request $rq) 
    {
      $data = array();
      if(JWTAuth::user() != null && JWTAuth::user() != "")
      {
        $data["status"] = true;
        $data["token"] = JWTAuth::getToken()->get();
        $data["credentials"] = Auth::user();
        $user_ins = new User();
        $data["profile"] = UsersProfile::where("userID", Auth::user()->id)->first();
      }else
      {
        $data["status"] = false;
      }

      return $data;
    }

    public function userTypes() 
    {
      $data = new UserTypes();
      $results = $data->get();
      return $results;
    }


    public function testStripe() 
    {
      $stripe = new Stripe();
      $customer = $stripe->customers()->create([
          'email' => Auth::User()->email,
      ]);

      echo $customer['id'];
    }

    public function createCard() 
    {
      $stripe = new Stripe();
      $token = $stripe->tokens()->create([
          'card' => [
              'number'    => '4242424242424242',
              'exp_month' => 10,
              'cvc'       => 314,
              'exp_year'  => 2020,
          ],
      ]);

      $card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);

      return $card;
    }
    public function userTitles() 
    {
      $data = new UserTitles();
      $results = $data->get();
      return $results;
    }

    public function sendPasswordForgot(Request $rq)
    {
      $check = new User();
      $checkRes = $check->where("email" , $rq->email)->get();

      if(count($checkRes) == 0) {
        $data = array();
        $data["status"] = false;
        $data["message"] = "Sorry email not found or not registered.";
        return $data;
      }


      $profile = UsersProfile::where("userID", $checkRes[0]->id)->first();


      $params = array();
      
      if($profile != null) 
      {
          if($profile->company_name == "")
          {
            if($profile->first_name == "") 
            {
              $params["to_name"] = $profile->email;
            }else{
              $params["to_name"] = $profile->first_name." ".$profile->last_name;
            }
          }else{
            $params["to_name"] = $profile->company_name;
          }
      }
      


      $params["verification_code"] = $this->generateRandomString(10);
      $params["user_id"] = $profile->userID;
      $params["subject"] = "Forgotten password reset.";
      $params["name"] = "Ethesia";
      $params["email_from"] = "donotreply@anesthesia.community";
      $params["to"] = $rq->email;
      $params["link"] = url("/")."/password/form-reset/".$params["verification_code"];


      $dev_emails = array("jetbuquematias@gmail.com" , "personal.ralphdegoma@gmail.com");

      if(env("APP_ENV") == "local" || env("APP_ENV") == "") 
      {
          $params["to"] = $dev_emails;
      }
      

      $user_verify = new VerificationUser();
      $user_verify->verification_code = $params["verification_code"];
      $user_verify->user_id = $params["user_id"];
      $user_verify->save();

      Mail::send(new SendPasswordResetMail($params));

      // Mail::send(['text'=>'emails.welcome_new_user'],['name'=>'jerome matias'],function($message){        
      //  $message->to('jetbuquematias@gmail.com','to jerome')->subject('test mail');
      //  $message->from('jetbuquematias@gmail.com','Jerome');            
      // });   

      $response = array();
      $response["status"] = true;
      $response["message"] = "Password reset has been sent.";
      return $response;
      //send email
    }

    public function newUser(Request $rq, ApplicationMessageNotifications $settings)
    {

      $temporary_password = $rq->email.$this->generateRandomString(6);


      ini_set('max_execution_time', 1000);

      $check = new User();
      $checkRes = $check->where("email" , $rq->email)->get();

      if(count($checkRes) > 0) {
        $data = array();
        $data["status"] = false;
        $data["message"] = "Sorry email is taken. Please choose another one.";
        return $data;
      }

      if($rq->password == "" && $rq->social == "")
      {
        $data = array();
        $data["status"] = false;
        $data["message"] = "password is required.";
        return $data;
      }

      if($rq->account_title == "") 
      {
        $data = array();
        $data["status"] = false;
        $data["message"] = "User Type is required.";
        return $data;
      }


      //check if from social
      if($rq->social == "google" || $rq->social == "fb") 
      {
        $password = $temporary_password;
      }else{
        $password = $rq->password;
      }

      $data = new User();
      $data->email = $rq->email;
      $data->password = Hash::make($password);
      $data->username = $rq->email;
      $data->userType = $rq->account_type;
      $data->userTitle = $rq->account_title;
      $data->is_confirmed = "no";
      $data->save();

      $users_profile = new UsersProfile();
      $users_profile->description = $rq->description;

      if($rq->account_type == 2) //company
      {
        $users_profile->company_name = $rq->company_name;
      }
      else
      {
        $users_profile->firstName = $rq->first_name;
        $users_profile->lastName = $rq->last_name;
      }


      Auth::attempt(['email' => $rq->email, 'password' => $password]);


      $users_profile->userID = Auth::User()->id;
      $users_profile->save();

      $profile = UsersProfile::where("userID", $data->id)->first();
      $params = array(); 


      $params["name"] = $rq->account_type == 2 ? $rq->company_name : $rq->first_name. " ".$rq->last_name;
      
      $params["url"] = url('/');
      $params["subject"] = "Welcome to Anesthesia Community";
      $params["email_from"] = "jeromematias@anesthesia.com";
      $params["to"] = $rq->email;

      // WelcomeNewUserJob::dispatch($params);
      $verification_code = $this->generateRandomString(10);
      
      $sendto = array(
        'name'=> $rq->account_type == 2 ? $rq->company_name : $rq->first_name. " ".$rq->last_name,
        'to' => $rq->email,
        'verification_code' => $verification_code,
        'link' => url("/")."/verify/".$verification_code."/".$users_profile->userID,
      );
      
      $user_verify = new VerificationUser();
      $user_verify->verification_code = $verification_code;
      $user_verify->user_id = Auth::User()->id;
      $user_verify->save();

      Mail::send(new SendNewUserMail($sendto));
      
      $data = array();
      $data["status"] = true;
      $data["credentials"] = Auth::user();
      $data["profile"] = $users_profile;
      $data["message"] = "Information has been saved.";


      /*add new contact labels here*/

      $label = array(
        array('user_id'=>Auth::User()->id,'contact_label'=>'Work','label_id'=>1),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Family','label_id'=>2),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Friends','label_id'=>3),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Private','label_id'=>4),
      );


      $settings->create(array(
        'user_id' => Auth::User()->id,
        'sms_application' => true,
        'sms_messages' => true,
        'sms_job_expiration' => true,
        'email_applicaton' => true,
        'email_messages' => true,
        'email_job_expiration' => true,
      ));

      $job_type = $rq->account_type == 1 ? "Candidate" : "Job Provider";

      $description = "A new $job_type user.";
      Auth::user()->ethesiaEvent($description);
      
      UserContactTypes::insert($label);

      return $data;

    }

    public function newPassword(Request $rq) 
    {
      $user = Auth::User();
      $response = array();
            
      // if($rq->password == "" || $rq->new_password == "") {
      //   //$response = array();
      //   $response["status"] = false;
      //   $response["message"] = "Password and new password is required.";
      //   return $response;
      // }      
      
      if(strlen(trim($rq->email)) == 0){
        $response["status"] = false;
        $response["message"] = "Please enter your email address.";
         return $response;
      }

      if(strlen($rq->new_password) > 0){
        if($rq->new_password != $rq->confirm_password) {          
          $response["status"] = false;
          $response["message"] = "Please confirmed your password. They are not the same.";
          return $response;
        }
      }      

      $check_email = User::where('email', $rq->email)->where('id', '!=', $user->id)->first();
      if($check_email){
        //$response = array();
        $response["status"] = false;
        $response["message"] = "Email is already taken.";
        return $response;
      }

      if(!Hash::check($rq->password, $user->password)){
        $response["status"] = false;
        $response["message"] = "Password is not correct. Please fill your previous password.";
        return $response;
      }

      if (Hash::check($rq->password, $user->password)) {
        $user = User::find($user->id);
        $user->email = $rq->email;
        $user->username = $rq->email;
        if(strlen($rq->new_password) > 0){        
          $user->password = Hash::make($rq->new_password);
        }
        $user->save();
        Auth::logout();
      }else{
        //$response = array();
        $response["status"] = false;
        $response["message"] = "Old password didnt matched.";
        return $response;
      }

      //$response = array();
      $response["status"] = true;
      $response["message"] = "Password has been updated.";
      return $response;
    }

    public function updateUser(Request $rq)
    {

      if($rq->id == "")
      {
        $data = new UsersProfile();
      }else
      {
        $data = UsersProfile::find($rq->id);
      }
      

      if(isset($rq["company_name"]))
      { 
        $data->company_name = $rq->company_name;
      }

      if(isset($rq["firstName"]))
      {
        $data->firstName = $rq->firstName;
      }

      if(isset($rq["lastName"]))
      {
        $data->lastName = $rq->lastName;
      }

      if(isset($rq["address"]))
      {
        $data->address = $rq->address;
      }

      if(isset($rq["city"]))
      {
        $data->city = $rq->city;
      }

      if(isset($rq["state"]))
      {
        $data->state = $rq->state;
      }

      if(isset($rq["country"]))
      {
        $data->country = $rq->country;
      }

      if(isset($rq["zipcode"]))
      {
        $data->zipcode = $rq->zipcode;
      }

      if(isset($rq["phone"]))
      {
        $data->phone = $rq->phone;
      }

      if(isset($rq["fax"]))
      {
        $data->fax = $rq->fax;
      }

      if(isset($rq["website"]))
      {
        $data->website = $rq->website;
      }

      if(isset($rq["emailPreference"]))
      {
        $data->emailPreference = $rq->emailPreference;
      }


      $data->userID = Auth::User()->id;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function updateBasicInfo(Request $rq) 
    {


      if($rq->id == "")
      {
        $data = new UsersProfile();
      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      if(isset($rq["minimumSalary"])) 
      {
        $data->minimumSalary = $rq->minimumSalary;
      }

      if(isset($rq["allowSearch"])) 
      {
        $data->allowSearch = $rq->allowSearch;
      }
      
      if(isset($rq["description"])) 
      {
        $data->description = $rq->description;
      }

      if(isset($rq["firstName"])) 
      {
        $data->firstName = $rq->firstName;
      }  

      if(isset($rq["lastName"])) 
      {
        $data->lastName = $rq->lastName;
      } 

      if(isset($rq["company_name"])) 
      {
        $data->company_name = $rq->company_name;
      }
      
      if(isset($rq->tag_line))
      {
        $data->tag_line = $rq->tag_line;
      }
      
      $data->address = $rq->address;
      $data->city = $rq->city;
      $data->state = $rq->state;
      $data->country = $rq->country;
      $data->zipcode = $rq->zipcode;
      $data->phone = $rq->phone;
      $data->fax = $rq->fax;
      $data->website = $rq->website;
      $data->emailPreference = $rq->emailPreference;

      $data->userID = Auth::User()->id;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function updateSocialInfo(Request $rq) 
    {
      if($rq->id == "")
      {

        $check = UsersProfile::where('userID',Auth::User()->id)->first();

        if($check == "") 
        {
          $data = new UsersProfile();
        }else
        {
          $data = UsersProfile::where('userID',Auth::User()->id)->first();
        }


      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      $data->facebookUrl = $rq->facebookUrl;
      $data->twitterUrl = $rq->twitterUrl;
      $data->youtubeUrl = $rq->youtubeUrl;
      $data->linkedinUrl = $rq->linkedinUrl;
      $data->googlePlusUrl = $rq->googlePlusUrl;
      $data->save();
      

      return $rq->linkedinUrl;
      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function removeEducationById(Request $rq) 
    {
      $data = UsersEducation::find($rq->id);
      $data->delete();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Successfully deleted.";
      return $response;
    }
    public function removeExperience(Request $rq)
    {
      $data = UsersExperience::find($rq->id);
      $data->delete();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Successfully deleted.";
      return $response;
    }
    public function getUserEducations()
    {
      $data = UsersEducation::where('userID',Auth::User()->id)->get();
      return $data;
    }
    public function getUserExperience() 
    {
      $data = UsersExperience::where('userID',Auth::User()->id)->get();
      return $data;
    }
    public function newEducation(Request $rq) 
    {
      if($rq->id != "") 
      {
        $data = UsersEducation::find($rq->id);
      }
      else
      {
        $data = new UsersEducation();
      }
    
      $data->userID = Auth::User()->id;
      $data->title_education = $rq->title_education;
      $data->from_education = $rq->from_education;
      $data->to_education = $rq->to_education;
      $data->school_name = $rq->school_name;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function newExperience(Request $rq) 
    {
      if($rq->id != "") 
      {
        $data = UsersExperience::find($rq->id);
      }
      else
      {
        $data = new UsersExperience();
      }
    
      $data->userID          = Auth::User()->id;
      $data->titleExperience = $rq->titleExperience;
      $data->fromExperience  = $rq->fromExperience;
      $data->toExperience    = $rq->toExperience;
      $data->companyName     = $rq->companyName;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function updateExtraInfo(Request $rq) 
    {
      if($rq->id == "")
      {

        $check = UsersProfile::where('userID',Auth::User()->id)->first();

        if($check == "") 
        {
          $data = new UsersProfile();
        }else
        {
          $data = UsersProfile::where('userID',Auth::User()->id)->first();
        }


      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      if($rq->file("file")[0] != null){

        $file = $rq->file("file")[0];      
      
        $name = $file->getClientOriginalName();      
        
        $data_obj = json_decode($rq->data);      
        
        $count = 1;        
        
        $id = Auth::User()->id;

        while(Storage::disk('public')->has("uploads/cover-letter/$id/$name")) {
            $name = $count."-".$name;
            $count++;
        }

        Storage::disk("public")->put("uploads/cover-letter/$id/$name",file_get_contents($rq->file("file")[0]));

        $data->resume_path = "uploads/cover-letter/$id/$name";
      }      
            
      $data->currentSalary = $data_obj->currentSalary;
      $data->expectedSalary = $data_obj->expectedSalary;
      $data->positionDuration = $data_obj->positionDuration;
      $data->coverLetter = $data_obj->coverLetter;
      $data->save();
      
      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function cardupdate(Request $rq)
    {
      
      
      if($rq->default)
      {
        $old = new UsersCreditCardDetails();
        $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

        foreach ($old_res as $key => $value) {
          $value->isDefault = "no";
          $value->save();
        }
      }

      $data = UsersCreditCardDetails::find($rq->id);
      $card = array();
            
      $user = Auth::User();
      
      $card["name"] = $rq->name;

      try {
          $card_id = $data['card_id'];
          $stripe = new Stripe();          

          $stripe->cards()->update($user->stripe_id, $card_id, $card);
      } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();
          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();
          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();

          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      }


      

      if($rq->default)
      {
        $data->isDefault = "yes";  
      }

      $data->name = $rq->name;
      $data->save();

    }
    public function updateUserCard(Request $rq)
    {

        if(App::environment('local')) 
        {
          putenv('STRIPE_API_KEY=sk_test_bQRvTILgvtm3puD0JXIb23jb');
        }else
        {
          putenv('STRIPE_API_KEY=sk_live_Onpva3jMwMZ1CZemzdZqHJDI');
        }
        

        if(Auth::User()->stripe_id == "") {
            $stripe = new Stripe();
            $customer = $stripe->customers()->create([
                'email' => Auth::User()->email,
            ]);

            if(Auth::User()->stripe_id == "") 
            {
              $user = Auth::User();
              $user->stripe_id = $customer["id"];
              $user->save();
            }

        }
    
        if($rq->id != "") 
        {
          $data = UsersCreditCardDetails::find($rq->id);
        }else
        {
          $data = new UsersCreditCardDetails();
        }
        

        $user = Auth::User();

        $card["name"] = $rq->name;
        $card["number"] = $rq->card_number;

        $month = $rq->exp_month;
        $year = $rq->exp_year;
        $card["exp_month"] = $month;
        $card["exp_year"] = $year;
        $card["cvc"] = $rq->cvc;

        if(isset($rq["address"])) {
          if($rq["address"] != "") {
            $card["address_line1"] = $rq->address;
            $data->address = $rq->address;
          }
        }

    /*    if(isset($rq["state"])) {
          if($rq["state"] != "") {
            $card["address_state"] = $rq->state;
            $data->state = $rq->state;
          }
        }*/

        if(isset($rq["zipcode"])) {
          if($rq["zipcode"] != "") {
            $card["address_zip"] = $rq->zipcode;
            $data->zipcode = $rq->zipcode;
          }
        }
    
        if($rq->id != "") 
        {
          try {
              $card_id = $rq->card_id;
              $stripe = new Stripe();

              unset($card["number"]);
              unset($card["cvc"]);

              $stripe->cards()->update($user->stripe_id, $card_id, $card);
          } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();
              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();
              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          }

          

        }else
        {
          
          try {


             

              $stripe = new Stripe();
              $token = $stripe->tokens()->create([
                  'card' => $card,
              ]);

              $card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);
              
              $data->card_id = $card["id"];
              $data->card_last_four = $card["last4"];
              $data->name = $card["name"];

              $default = "no";

              if($rq->isDefault == true) 
              {
                $default = "yes";
              }
              if($rq->isDefault == false) 
              {
                $rq->isDefault = "no";
              }

              if($default == "yes") 
              {
                $stripe = new Stripe();
                $stripe->customers()->update(Auth::User()->stripe_id, [
                    'default_source' => $data->card_id,
                ]);
              }
              
          } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
              
          } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          }
          
        }
      
      if(isset($rq["isDefault"]))
      {
        if($rq->isDefault == true) {
          $rq->isDefault = "yes";

          $old = new UsersCreditCardDetails();
          $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

          foreach ($old_res as $key => $value) {
            $value->isDefault = "no";
            $value->save();
          }

        }
        if($rq->isDefault == false) {
          $rq->isDefault = "no";
        }
        
      }

      $data->userID = Auth::User()->id;
      $data->isDefault = $rq->isDefault;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function updateUserECheck(Request $rq)
    {

      if($rq->id == "")
      {
        $data = new UsersECheckDetails();
      }else
      {
        $data = UsersECheckDetails::find($rq->id);
      }
      
      if(isset($rq["name"]))
      { 
        $data->name = encrypt($rq->name);
      }


      if(isset($rq["bankAccountNumber"]))
      { 
        $data->bankAccountNumber = encrypt($rq->bankAccountNumber);
      }

      if(isset($rq["routingNumber"]))
      { 
        $data->routingNumber = encrypt($rq->routingNumber);
      }


      if(isset($rq["isDefault"]))
      {
        if($rq->isDefault == true) {
          $rq->isDefault = "yes";

          $old = new UsersECheckDetails();
          $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

          foreach ($old_res as $key => $value) {
            $value->isDefault = "no";
            $value->save();
          }
        }
        if($rq->isDefault == false) {
          $rq->isDefault = "no";
        }
        $data->isDefault = $rq->isDefault;
      }



      $data->userID = Auth::User()->id;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;

    }

    public function getProfile() 
    {
      $data = UsersProfile::where("userID", Auth::User()->id)->first();

      if($data == null || $data == "") 
      {
        $data = array();
        $data["image_path"] = "/public/images/default.png";
      }else 
      {
        $pic_array = explode("/", $data["image_path"]);
        $pic_name = $pic_array[count($pic_array) - 1];
        if(!$this->checkProfilePicExist(Auth::user()->id , $pic_name))
        {
          $data["image_path"] = "/public/images/default.png";
        }
        else
        {
          $data["image_path"] = url('/').$data->image_path;
        }
        $data['userType'] = $data->user->userType;    
        $data['fullname'] = $data->full_name;
      }
      
      return $data;
    }
    public function getBillingDetails($id) 
    { 
      putenv('STRIPE_API_KEY=sk_live_Onpva3jMwMZ1CZemzdZqHJDI');

      $stripe               = new Stripe();
      $user                 = Auth::User();
      $data                 = UsersCreditCardDetails::find($id);
      $card                 = $stripe->cards()->find($user->stripe_id, $data->card_id);
      $data->name           = $card["name"];
      $data->last_card_four = "********".$card["last4"];
      $data->cardIDNumber   = "****";
      $data->address        = $card["address_line1"];
      return $data;
    }

    public function getCreditCards() {
      $data = UsersCreditCardDetails::where("userID", Auth::User()->id)->get();

      foreach ($data as $key => $value) {
        $value->name       = $value->name;
        $value->card_last_four = "*******".$value->card_last_four;
      }
      return $data;
    }

    public function getCheckList() 
    {
      putenv('STRIPE_API_KEY=sk_live_Onpva3jMwMZ1CZemzdZqHJDI');
      $data = UsersECheckDetails::where("userID", Auth::User()->id)->get();
      foreach ($data as $key => $value) {
        $value->name              = decrypt($value->name);
        $value->routingNumber     = decrypt($value->routingNumber);
        $value->bankAccountNumber = decrypt($value->bankAccountNumber);
      }
      return $data;
    }

    public function removeCard($id)
    {
      putenv('STRIPE_API_KEY=sk_live_Onpva3jMwMZ1CZemzdZqHJDI');
      $stripe = new Stripe();
      $user = Auth::User();
      $data = UsersCreditCardDetails::find($id);
      $stripe->cards()->delete($user->stripe_id, $data->card_id);
      $data->forceDelete();
      $response = array();
      $response["status"] = true;
      $response["message"] = "deleted.";
      return $response;
    }

    public function getProfileImageForPdf($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) {
            $storagePath = $_SERVER['DOCUMENT_ROOT']."/"."public/images/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }

            $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            return $file->response($type);
        }

        $storagePath = storage_path()."/app/public/uploads/profiles/$id/$name";
        $file = File::get($storagePath);
        $type = pathinfo($storagePath);
        $type = $type["extension"];

        $file = Image::make($storagePath)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

        return $file->response($type);
    }


    public function checkProfilePicExist($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) 
      {
            $storagePath = $_SERVER['DOCUMENT_ROOT']."/"."public/images/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }

            return false;
        }
        else
        {
            return true;
        }

      
    }

    public function getProfileImage($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) {
            $storagePath = storage_path()."/app/public/uploads/profiles/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }


            $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            return $file->response($type);
        }

        $storagePath = storage_path()."/app/public/uploads/profiles/$id/$name";
        $file = File::get($storagePath);
        $type = pathinfo($storagePath);
        $type = $type["extension"];

        $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

        return $file->response($type);
    }

    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function uploadPhoto(Request $rq) {

      $file = $rq->file("image");
      $file_ex = ".png";
      $name = $this->generateRandomString(6).".".$file_ex;

      $count = 1;

      $id = Auth::User()->id;

      while(Storage::disk('public')->has("uploads/profiles/$id/$name")) {
          $name = $count."-".$name;
          $count++;
      }

      Storage::disk("public")->put("uploads/profiles/$id/$name",file_get_contents($rq->file("image")));

      $storagePath_raw = "/user/profile/$id/$name";
      $storagePath = url('/').$storagePath_raw;

      $user = UsersProfile::where("userID", $id)->first();

      if($user == null || $user == "") 
      {
        $user = new UsersProfile();
        $user->userID = $id ;
      }

      $user->image_path =  $storagePath_raw;
      $user->save();

      $data = array("success" => true, "path" => $storagePath);

      return $data;

    }

    public function removeChecks($id) 
    {
      UsersECheckDetails::find($id)->forceDelete();
      $response = array();
      $response["status"] = true;
      $response["message"] = "deleted.";
      return $response;
    }

    public function getEchecksDetails() 
    {
      $data = UsersECheckDetails::where("userID", Auth::User()->id)->first();
      $data->name              = decrypt($data->name);
      $data->routingNumber     = decrypt($data->routingNumber);
      $data->bankAccountNumber = decrypt($data->bankAccountNumber);
      return $data;
    }

    public function getDashboard(){
      $setting = JobPostSettings::first();
      //var_dump($setting->free_posting);
      //return;
      $user = User::where("id", Auth::User()->id)->first();      
      $applicant_count = 0;
      $jobs_id = array();      
      $current_period = date('Y-m');      
      //$jobs = $user->jobs()->where(DB::raw("DATE_FORMAT(creation_date,'%Y-%m')"), $current_period)->where('post_status', 'active')->get();      
      $jobs = $user->jobs()->where('post_status', 'active')->get();
      //$jobs_type = $user->jobs()->where(DB::raw("DATE_FORMAT(creation_date,'%Y-%m')"), $current_period)->get();
      //$jobs_types = $user->jobs()->groupBy('jobPostType')->selectRaw('jobPostType, count(*) as total')->where('post_status', 'active')->where(DB::raw("DATE_FORMAT(creation_date,'%Y-%m')"), $current_period)->get();
      $jobs_types = $user->jobs()->groupBy('jobPostType')->selectRaw('jobPostType, count(*) as total')->where('post_status', 'active')->get();
      $jobs_top_location = $user->jobs()->where('post_status', 'active')->groupBy('ei_facilityState')->selectRaw('ei_facilityState, count(*) as total')
        //->where(DB::raw("DATE_FORMAT(creation_date,'%Y-%m')"), $current_period)
        ->where('ei_facilityState', '!=', 'All')
        ->limit(3)
        //->orderBy('total', 'desc')->get();
        ->orderBy('total', 'desc')
        ->orderBy('ei_facilityState', 'asc')->get();      
      
      $color = array('#1e88e5', '#fc4b6c', '#26c6da', '#1e88e5');
      $top_locations = array();
      $index = 0;
      foreach($jobs_top_location as $job_top_location){
        //$top_locations[$job_top_location->ei_facilityState] = $job_top_location->total;
        /*
        $state = $job_top_location->ei_facilityState;
        if(strlen($state) == 2){
          $state = $this->states[$state];                    
        }
        */
        //$top_locations[] = array('state' => $job_top_location->ei_facilityState, 'total' => $job_top_location->total, 'color' => $color[$index++]);
        $state = $job_top_location->ei_facilityState;
        $top_locations[] = array('state' => $state, 'total' => $job_top_location->total, 'color' => $color[$index++]);        
      }

      $plan_usage_post_type = array('anesthesiologist' => 0, 'crna' => 0);
      foreach($jobs_types as $jobs_type){        
        if($jobs_type->jobPostType == 1){
          $plan_usage_post_type['anesthesiologist'] = $jobs_type->total;
        }else if($jobs_type->jobPostType == 2){
          $plan_usage_post_type['crna'] = $jobs_type->total;
        }
      }      
      
      foreach($user->jobs as $job){      
        $jobs_id[] = $job->id;        
        $applicant_count += count($job->inqueries);        
      }            
      $application_latest = UserInquiries::whereIn('job_id', $jobs_id)->orderBy('created_at', 'desc')->limit(3)->get();     
      //dd($application_latest); 
      $application_count = UserInquiries::whereIn('job_id', $jobs_id)->where(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"), $current_period)->count();
      $application_count_all = UserInquiries::whereIn('job_id', $jobs_id)->count();
      $applicant_by_month = UserInquiries::whereIn('job_id', $jobs_id)->groupBy('month')->selectRaw("DATE_FORMAT(created_at,'%Y-%m') as month, count(*) as total")->orderBy('created_at', 'desc')->get();      
      $applicants_by_month = array();
      $applicants_by_month_index = 1;
      $applicants_total = 0;

      $applicants_by_month[0] = array('month_year' => 'All Time', 'count' => $application_count_all);
      //$applicants_current_month_is_exist = false;

      foreach($applicant_by_month as $applicant_by_month_){
        $date_format = date('F Y', strtotime($applicant_by_month_->month));        
        $applicants_by_month[$applicants_by_month_index++] = array(
          'month_year' => $date_format,
          'count' => $applicant_by_month_->total
        );        
      }         
      $jobs_count = count($jobs);            
      $applicants = array();


      foreach($application_latest as $application){

        if($application->user != null) 
        {
          $downloadable_link = url("/")."/downloads/resume/". $application->profile['userID'] ."/$application->cv_file_name";
          
          if($application->user->userProfile->image_path == "")
          {
            $image_path = Avatar::create( ucfirst($application->user->userProfile->firstName) . " " . ucfirst($application->user->userProfile->lastName) )->toBase64()->encoded; 
          }else{
            $image_path = $application->user->userProfile->image_path;
          }
        }else
        {
          $image_path = Avatar::create( ucfirst($application->applicant_name) )->toBase64()->encoded; 
          $downloadable_link = url("/")."/downloads/resume/$application->cv_file_name";
        }
         
        $applicants[] = array(
          'id' => $application->id,
          'user_id' => $application->user != null ? $application->user->id : "",
          'name' => $application->user != null ? $application->user->userProfile->firstName.' '.$application->user->userProfile->lastName : $application->applicant_name,
          'message' => $application->message,
          'date' => $application->created_at->format('M. d Y H:i a'),
          'profile_image' => $image_path ,
          'cv_file_name' => $application->cv_file_name,
          'downloadable_link' => $downloadable_link,
          'status' => $application->status
        );
      }

      try{ 
        if($setting->free_posting == 1){
          $subscribe_plan = postingPlans::find(3);
        }else{
          $subscribe_plan = $user->plans()->where('status', 'active')->first()->plan;
        }        
      }
      catch(\Exception $ex){}

      try{ 
        $limit = ($subscribe_plan->is_unlimited == 'yes') ? -1 : $subscribe_plan->post_allowed;         
      }
      catch(\Exception $ex){ 
        $limit = 10;         
      }

      if($limit == -1){
        //$plan_percentage = $plan_usage_post_type['crna'] .'/'. $plan_usage_post_type['anesthesiologist'];
        $plan_percentage = $jobs_count;
      }else{
        try{ 
          $plan_percentage = ($jobs_count * 100) / $limit; 
          $plan_percentage .= '%';
        }
        catch(\Exception $ex){ $plan_percentage = 0; }
      }      

      $data['jobs_count'] = $jobs_count;
      $data['applicants'] = $applicants;
      //$data['application_count'] = $application_count;            
      $data['application_count'] = $application_count_all;
      
      $data['limit'] = $limit;
      try{ $data['plan'] = $subscribe_plan->plan.' Plan Overview'; }
      catch(\Exception $ex){ $data['plan'] = 'No Plan'; }     
      $data['year_month'] = date('F Y');
      //$data['year_month'] = 'September 2018';      
      $data['plan_usage_post_type'] = $plan_usage_post_type;
      $data['plan_percentage'] = $plan_percentage;
      $data['top_locations'] = $top_locations;
      $data['applicants_by_month'] = $applicants_by_month;
      if($limit == -1){
        $data['jobs_post_available'] = 10;
      }else{
        $data['jobs_post_available'] = $limit - $jobs_count;
      }
      
      
      return $data;
    }
    public function updateUserInquiries(Request $request, $id){
      $user_inquiry = UserInquiries::find($id);
      $user_inquiry->fill($request->all())->save();

      return $user_inquiry;
    }    
}
