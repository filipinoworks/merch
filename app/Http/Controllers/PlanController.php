<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\OldPosts;
use App\UsersProfile;
use App\OldUsers;
use App\User;
use App\JobStatusHistory;
use App\EthesiaEvent;
use Hash;
use DB;
use Auth;
use Carbon\Carbon;
use App\Plan;
use App\PlanUser;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PlanController extends Controller
{
    //

    public function index(Request $request)
    {	
    	$plans = Plan::all();

    	return response($plans, 200);
    }

    public function activatePlan(Request $request, PlanUser $plan_user_ins) 
    {
    	return $plan_user_ins->activatePlan($request->all());
    	return response("Plan is activated", 200);
    }

}
