<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesChannel;
use JWTAuth;
use App\InitializeSalesChannel;


class SalesChannelController extends Controller
{
    //

	public function show(SalesChannel $sales_channel, $id)
    {
    	return $sales_channel::find($id);
    }
	public function index()
    {
        $sales_channel_ins = new SalesChannel();
        return $sales_channel_ins->getSalesChannels();
    }
    public function store(Request $rq, SalesChannel $sales_channel)
    {
    	$sales_channel->saveChannel($rq->all());

    	return response()->json("Changes has been saved.", 200); 
    }




}
