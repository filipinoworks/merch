<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Research;
use App\SalesChannel;
use App\Listing;

class ResearchController extends Controller
{
    //

    public function createListing($id)
    {
        $sales_channel_ins = new SalesChannel();
        $sales_channels =  $sales_channel_ins->getSalesChannels();

        $research_idea = Research::find($id);

        $data                = array();
        $data["title"]       = $research_idea->design_phrase;
        $data["keywords"]    = $research_idea->keys;
        $data["notes"]       = $research_idea->notes;
        $data["path_code"]   = $research_idea->path_code;
        $data["description"] = "";
        $data["is_bind"]     = true;
        $data["brand"]       = "";
        $data["designer"]    = "";
        $data["bp_one"]      = "";
        $data["bp_two"]      = "";
        $data["bp_one"]      = "";

        $data["products"] = array();
        foreach ($sales_channels as $key => $sales_channel) {
            foreach ($sales_channel["products"] as $key => $product) {
                array_push($data["products"], $product);
            }
        }

    	$listing_ins = new Listing();
    	$listing_ins = $listing_ins->storeListing($data);

        return response("Listing has been created.", 200);
    }
	public function updateStatus(Request $rq) 
	{
		$research_ins = Research::find($rq->id);
		$research_ins->status = $rq->status;
		$research_ins->save();

		return response("Research idea is now finished.", 200);
	}
	public function show($id)
	{
		$ins = new Research();
    	$research = $ins->find($id);
    	$photos = $ins->getPhotos($research->path_code);

    	return array("research" => $research, "photos" => $photos);
	}

    public function store(Request $rq) 
    {
    	$research = new Research();
    	$research->store($rq->all());

    	return response("Research data has been saved.", 200);
    }

	public function getFinishedResearch()
    {
    	$ins = new Research();
    	return $ins->getAllByStatus("finished");
    }
    public function getNewResearch()
    {
    	$ins = new Research();
    	return $ins->getAllByStatus("new");
    }

    public function delete($id)
    {
    	$research_ins = Research::find($id);
    	$research_ins->remove();
    	return response("Research has been removed.", 200);
    }
}
