<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\User;

class TeamMember extends Model
{
    //
    public function member() 
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function currentUserHasAccess($user_id)
    {	

    	$owner_has_access = TeamMember::where("user_id", $user_id)->with("team", "team.owner")->first();
		return JWTAuth::user()->id == $owner_has_access->team->owner->id;
    }

    public function getDetails($user_id)
    {
    	if($this->currentUserHasAccess($user_id))
    	{
    		$user_ins = User::where("id", $user_id)->with("userProfile")->first();

    		if(count($user_ins->roles) > 0) 
    		{
    			$user_ins->role = $user_ins->roles->pluck('name')[0];
    		}

    		return $user_ins;

    	}else
    	{
    		return response("Sorry not authorized.", 403);
    	}
    }

    public function team() 
    {
        return $this->belongsTo('App\Team', 'team_id');
    }
}
