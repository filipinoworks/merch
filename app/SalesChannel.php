<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\SalesChannel;
use JWTAuth;


class SalesChannel extends Model
{
    //
    use SoftDeletes;
    protected $table = "sales_channel";
    protected  $fillable = ["id"];

    public function getSalesChannels() 
    {
        $sales_channel =  SalesChannel::where("user_id" , JWTAuth::user()->id)->orwhere("user_id" , 0)
                    ->with("products")
                    ->with("products.product")
                    ->get();
      
        return $sales_channel;
    }

    public function saveChannel($data)
    {
    	$sales_chan_ins = SalesChannel::firstOrCreate(['id' => isset($data['id']) ? $data['id'] : ""]);
    	$sales_chan_ins->user_id = JWTAuth::user()->id;
    	$sales_chan_ins->channel_name = $data["channel_name"];
    	$sales_chan_ins->supports_upc_sku = isset($data["supports_upc_sku"]) ? $data["supports_upc_sku"] : 0;
    	$sales_chan_ins->default = isset($data["default"]) ? $data["default"] : 0;
    	$sales_chan_ins->save();

    	return $sales_chan_ins;

    }

    public function products() 
    {
        return $this->hasMany('App\SalesChannelProduct', 'sales_channel_id');
    }
}
