<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JWTAuth;
use Carbon\Carbon;
use App\ListingProduct;
use App\ListingChannel;
use App\SalesChannel;
use App\ListingPhotoPath;

class Listing extends Model
{
    //
	use SoftDeletes;
    protected $table = "listings";


    public function products() 
    {
    	return $this->hasMany('App\ListingProduct', 'listing_id');
    }
    public function getSalesChannels($products) 
    {
    	$sales_channels = array();

    	foreach ($products as $key => $value) 
    	{
    		$sales_channels[$value->sales_channel_id]["channel_name"] = $this->getChannelName($value->sales_channel_id);
    		$value->selected_colors = $value->selected_colors;
            $sales_channels[$value->sales_channel_id]["products"][] = $value;
    	}

    	return $sales_channels = array_values($sales_channels);
    }
    public function getChannelName($sales_channel_id)
    {
    	$channel_name = "";

    	$channel = SalesChannel::find($sales_channel_id);
        $channel_name = $channel->channel_name;

    	return $channel_name;
    }

    public function getPhotos($path_code)
    {   
        $photos_ins = ListingPhotoPath::where("path_code", $path_code)->get();
        return $photos_ins;
    }

    public function getListing()
    {

    	$listing_ins = $this->where("user_id", JWTAuth::user()->id)->with("products")
                    ->orderBy("id", "desc")
                    ->get();
    	$sales_channels = [];

    	foreach ($listing_ins as $key => $list) {
			$list->added = $list->created_at->format('d/m/Y');
			$list->updated = $list->updated_at->format('d/m/Y');
			$list->sales_channels = $this->getSalesChannels($list->products);
            $list->photos = $this->getPhotos($list->path_code);
            $list->designer_name = $this->designer($list->designer);
            $list->creator = $this->creator($list->user_id);
            unset($list["products"]);
    	}



    	return $listing_ins;
    }

    public function designer($id)
    {
        return User::where("id" , $id)->with("userProfile")->first();
    }
    public function creator($id)
    {
        return User::where("id" , $id)->with("userProfile")->first();
    }
    public function generateRandomString($length) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    


    public function storeListing($data)
    {
    	unset($data["token"]);

		if(isset($data["id"]) && $data["id"] != "")
    	{
    		$listing_ins = Listing::find($data["id"]);
    	}else
    	{

    		$listing_ins = new Listing();
    	}

        $listing_ins->user_id               = JWTAuth::user()->id;
        $listing_ins->brand                 = $data["brand"];
        $listing_ins->notes                 = isset($data["notes"]) ? $data["notes"] : "";
        $listing_ins->title                 = $data["title"];
        $listing_ins->designer              = isset($data["designer"]) ? $data["designer"] : null;
        $listing_ins->input_designer        = isset($data["input_designer"]) ? $data["input_designer"] : null;
        $listing_ins->unregistered_designer = isset($data["unregistered_designer"]) ? $data["unregistered_designer"] : null;
        $listing_ins->keywords              = $data["keywords"];
        $listing_ins->bp_one                = $data["bp_one"];
        $listing_ins->bp_two                = $data["bp_two"];
        $listing_ins->is_bind               = $data["is_bind"];
        $listing_ins->description           = $data["description"];
        $listing_ins->path_code             = $data["path_code"];
		$listing_ins->save();

		foreach ($data["products"] as $key => $product) 
		{
			if(isset($product["id"]) && $product["id"] != "")
			{
				$listing_prod_ins = ListingProduct::find($product["id"]);
			}else
			{
				$listing_prod_ins = new ListingProduct();
			}


            $listing_prod_ins->src_product_id   = $product["src_product_id"];
            $listing_prod_ins->listing_id       = $listing_ins->id;
            $listing_prod_ins->price            = isset($product["price"]) ? $product["price"] : "";
            $listing_prod_ins->link             = isset($product["link"]) ? $product["link"] : "";
            $listing_prod_ins->sales_channel_id = isset($product["sales_channel_id"]) ? $product["sales_channel_id"] : "";
            
            if(isset($product["selected_colors"]))
            {
                $listing_prod_ins->setAttribute("selected_colors", $product["selected_colors"]);
            }
            
            $listing_prod_ins->setAttribute("light_colors", $product["light_colors"]);
            $listing_prod_ins->setAttribute("dark_colors", $product["dark_colors"]);
            $listing_prod_ins->setAttribute("default_colors", $product["default_colors"]);
            

            $listing_prod_ins->selected_color_profile  = isset($product["selected_color_profile"]) ? $product["selected_color_profile"] : "";
            $listing_prod_ins->product_name     = isset($product["product_name"]) ? $product["product_name"] : "";
            $listing_prod_ins->is_light         = isset($product["is_light"]) ? $product["is_light"] : "";
            $listing_prod_ins->title            = isset($product["title"]) ? $product["title"] : "";
            $listing_prod_ins->brand            = isset($product["brand"]) ? $product["brand"] : "";
            $listing_prod_ins->bp_one           = isset($product["bp_one"]) ? $product["bp_one"] : "";
            $listing_prod_ins->bp_two           = isset($product["bp_two"]) ? $product["bp_two"] : "";
            $listing_prod_ins->description      = isset($product["description"]) ? $product["description"] : "";
            $listing_prod_ins->save();


		}
    }


    public function user()
    {
        return $this->belongsTo('App\Listing', 'user_id');
    } 
}
