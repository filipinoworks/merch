<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\ListingPhotoPath;

class ListingPhotoPath extends Model
{
    //

    public function hasAccess($id)
    {	
    	$user = JWTAuth::user();
    	$photo_ins = ListingPhotoPath::where('id', $id)->with('listing','listing.user','research')->first();
    	if(isset($photo_ins["listing"])) 
    	{
    		return $user->id == $photo_ins->listing->user_id;
    	}elseif(isset($photo_ins["research"]))
    	{
    		return $user->id == $photo_ins->research->user_id;
    	}
    	
    }
    public function research()
    {
        return $this->belongsTo('App\Research', 'path_code', 'path_code');
    }
    public function listing()
    {
        return $this->belongsTo('App\Listing', 'path_code', 'path_code');
    }
}
