<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\ListingPhotoPath;

class Research extends Model
{
    //

    protected $table = "researchs";

    public function getPhotos($path_code)
    {
    	return $listing = ListingPhotoPath::where("path_code", $path_code)->get();
    }
    public function hasAccess($user_id)
    {
    	$user = JWTAuth::user();
    	return $user->id == $user_id;
    }
    public function remove() 
    {
    	if($this->hasAccess($this->user_id))
    	{
    		$this->delete();
    	}
    }

    public function getAllByStatus($status)
    {
    	$ins = $this->where("status", $status)->where("user_id", JWTAuth::user()->id)->get();
    	return $ins;
    }

    public function store($data)
    {
    	if(isset($data['id']) && $data['id'] != "")
    	{
    		$ins  = Research::find($data["id"]);
    	}else
    	{
    		$ins                = new Research();
    	}

		$ins->path_code     = $data["path_code"];
		$ins->user_id       = JWTAuth::user()->id;
		$ins->design_phrase = $data["design_phrase"];
		$ins->link          = $data["link"];
		$ins->keys          = $data["keys"];
		$ins->notes         = $data["notes"];
		$ins->save();

	}
}
