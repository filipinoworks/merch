<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesChannelProduct extends Model
{
    //
    use SoftDeletes;
    protected $table = "sales_channel_products";
    protected  $fillable = ["id"];

    public function product()
    {
    	return $this->belongsTo('App\Product', 'product_id');

    }

    public function getSalesProducts($rq)
    {
        $products = SalesChannelProduct::where("sales_channel_id", $rq["sales_channel_id"])->with('product')->get();
        foreach ($products as $key => $product) 
        {
            if($product->src_product_id != null)
            {
                $product_src = SalesChannelProduct::find($product->src_product_id);
                $product->product_name = $product_src->product_name;  
            }
            
        }

        return $products;

    }

    public function saveProduct($data)
    {

        $ins                   = SalesChannelProduct::firstOrCreate(['id' => isset($data['id']) ? $data['id'] : ""]);
        $ins->light_colors     = json_encode($data["light_colors"]);
        $ins->dark_colors      = json_encode($data["dark_colors"]);
        $ins->sales_channel_id = $data["sales_channel_id"];
        $ins->price            = str_replace("$","",$data["price"]);
    	
        //$ins->product_id = $data["product_id"];
        //$ins->max_colors = $data["max_colors"];
        
        $ins->product_name = $data["product_name"];
        $ins->title        = $data["title"];
        $ins->bp_one       = $data["bp_one"];
        $ins->bp_two       = $data["bp_two"];
        $ins->description  = $data["description"];
        $ins->save();

    	return $ins;

    
    }
}
