<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use Carbon\Carbon;

class PlanUser extends Model
{
    protected $table = "plan_users";



    public function activatePlan($data) 
    {

    	$check_ins = new $this;
    	$check_ins->where("user_id", JWTAuth::user())->get();

    	$current_date = date('Y-m-d H:i:s');
    	$user = JWTAuth::user();
    	$ins = new $this;
    	$ins->plan_id = $data["plan"]["id"];
    	$ins->user_id = $user->id;
    	$ins->is_active = true;
    	$ins->date_subscribed = $current_date;
    	$ins->save();
    }
}