<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\SalesChannel;
class InitializeSalesChannel extends Model
{
    //

    public function init()
    {
    	$default_sales_channels = SalesChannel::where("default", true)
                    ->with("products")
                    ->get();


    	foreach ($default_sales_channels as $key => $channel) {

	    	$sales_chan_ins = new SalesChannel();
	    	$sales_chan_ins->user_id = JWTAuth::user()->id;
	    	$sales_chan_ins->channel_name = $channel->channel_name;
	    	$sales_chan_ins->supports_upc_sku = false;
	    	$sales_chan_ins->default = true;
	    	$sales_chan_ins->save();

	    	$products = $channel->products;

		    foreach ($products as $key => $product) {	
		    	
		    	$dark_colors = json_decode($product->dark_colors);
	    		$light_colors = json_decode($product->light_colors);

		    	$ins = new SalesChannelProduct();
		    	$ins->light_colors = json_encode($light_colors);
		    	$ins->dark_colors = json_encode($dark_colors);
		    	$ins->sales_channel_id = $sales_chan_ins->id;
		    	$ins->price = $product->price;
		    	
		    	//$ins->product_id = $data["product_id"];
		        //$ins->max_colors = $data["max_colors"];
		        $ins->src_product_id = $product->id;
		        $ins->product_name = $product->product_name;
		        $ins->title = $product->title;
		        $ins->bp_one = $product->bp_one;
		        $ins->bp_two = $product->bp_two;
		        $ins->description = $product->description;
		    	$ins->save();
	        }
    	}
        

       

    }
}
