<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use JWTAuth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    //
    use SoftDeletes;
    protected $table = "products";

    public function storeProduct($data)
    {
    	if(isset($data["id"]) && $data["id"] != "")
    	{
	    	$product = Product::find($data["id"]);
	    }else
	    {
	    	$product = new Product();
	    }

    	$product->user_id = JWTAuth::user()->id;
    	$product->product_name = $data["product_name"];
    	$product->title = $data["title"];
    	$product->description = isset($data["description"]) ? $data["description"] : "";
    	$product->bp_one = isset($data["bp_one"]) ? $data["bp_one"] : "";
    	$product->bp_two = isset($data["bp_two"]) ? $data["bp_two"] : "";
        $product->default_listing = isset($data["default_listing"]) ? $data["default_listing"] : 0;
    	$response = $product->save();
	    

    	return $response;
    }
}
