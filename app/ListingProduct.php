<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingProduct extends Model
{
    //

    public function getDefaultColorsAttribute($value)
    {
        return json_decode($value);
    }

    public function getLightColorsAttribute($value)
    {
        return json_decode($value);
    }

    public function getDarkColorsAttribute($value)
    {
        return json_decode($value);
    }

    public function getSelectedColorsAttribute($value)
    {
        return json_decode($value);
    }

    public function setSelectedColorsAttribute($value)
    {
    	if(!is_string($value)){
        	$this->attributes['selected_colors'] = json_encode($value);
    	}else{
    		$this->attributes['selected_colors'] = $value;
    	}
    }

    public function setDefaultColorsAttribute($value)
    {
    	if(!is_string($value)){
        	$this->attributes['default_colors'] = json_encode($value);
    	}else{
    		$this->attributes['default_colors'] = $value;
    	}
    }

    public function setLightColorsAttribute($value)
    {
    	if(!is_string($value)){
        	$this->attributes['light_colors'] = json_encode($value);
    	}else{
    		$this->attributes['light_colors'] = $value;
    	}
    }

    public function setDarkColorsAttribute($value)
    {
    	if(!is_string($value)){
        	$this->attributes['dark_colors'] = json_encode($value);
    	}else{
    		$this->attributes['dark_colors'] = $value;
    	}
    }


}
