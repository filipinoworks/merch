<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersProfile extends Model
{
    protected $table = "users_profile";
    
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['description', 'company_name', 'firstName', 'lastName', 'userID','address', 'phone'];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userID');
    }

    public function getFullNameAttribute()
    {
        return "{$this->firstName} {$this->lastName}";
    }
}
