<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs;

class JobStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:status 
                                {status=backup : this is the new status of the jobs} 
                                {where=deleted : status of the jobs to update.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update job status...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = $this->argument('status');
        $where = $this->argument('where');
        
        Jobs::where('post_status', $where)->update(['post_status' => $status]);

        $this->info('Post has deleted status was changed to backup....');
    }
}
