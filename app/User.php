<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;
use Carbon\Carbon;
use Avatar;
use Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Hash;
use JWTAuth;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = 'api';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function makeUser($rq, $team_register = false)
    {
        if(isset($rq["id"]) && $rq["id"] != "") 
        {
            $data = User::find($rq["id"]);
            $users_profile = UsersProfile::where("userID", $rq["id"])->first();
            if(isset($rq["password"]) && $rq["password"] != "") 
            {
                $data->password = Hash::make($rq["password"]);
            }
            
        }else
        {
            $data = new User();
            $users_profile = new UsersProfile();
            $data->password = Hash::make($rq["password"]);
        }

        $data->email = $rq["email"];
        $data->username = $rq["email"];
        $data->is_confirmed = "yes";
        $data->save();

        
        $users_profile->firstName = $rq["firstName"];
        $users_profile->lastName = $rq["lastName"];
        $users_profile->userID = $data["id"];
        $users_profile->save();

        $credentials = request(['email', 'password']);
        $token = "";

        $profile = UsersProfile::where("userID", $data["id"])->first();
        $user_data = User::find($data["id"]);
        

        //assign role

        if(!$team_register)
        {
            $user_data->assignRole("Owner");
        }else
        {   
            if($user_data->roles->first() != null) 
            {
                $user_data->removeRole($user_data->roles->first());
            } 
            
            $user_data->assignRole($rq["role"]);
        }
        

        $data = array();
        $data["status"] = true;
        $data["token"] = $token;
        $data["credentials"] = $user_data;
        $data["profile"] = $profile;
        return $data;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    protected $fillable = [
        'name', 'email', 'password', 'username', 'userType', 'userTitle', 'is_confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];    
    public function userProfile()
    {
        return $this->hasOne('App\UsersProfile', 'userID');
    }
    public function userAccountTitle()
    {
        return $this->hasOne('App\UserTitles', 'id','userTitle');
    }
    public function userJobProviderBookmark()
    {
        return $this->hasOne('App\JobProvider', 'jobProviderId','id');
    }
    public function UserContactList()
    {
        return $this->hasOne('App\UserContactList', 'user_contact_id', 'id','user_id','id');
    }
    public function bookmark()
    {
        return $this->hasMany('App\JobBookmark', 'userId');
    }

    public function getTimeAgo($carbonObject) 
    {
        return $carbonObject->diffForHumans();
    }
    public function getName()
    {   
        //candidate
        if($this->userType == 1)
        {
            return $this->userProfile["firstName"]. " " .$this->userProfile["lastName"];   
        }
        else
        {
            return $this->userProfile["company_name"];
        }
    }

   

    public function getImageEvent($name)
    {   
        return Avatar::create(ucfirst($name))->toBase64()->encoded;   
    }


    public function getImage()
    {   
        //candidate
        if($this->userProfile["image_path"] == "")
        {
            if($this->userAdmin == 1) 
            {
                return Avatar::create(ucfirst('Admin'))->toBase64()->encoded;   
            }
            return $this->userType == 1 ? Avatar::create(ucfirst($this->userProfile["firstName"]) . " " . ucfirst($this->userProfile["lastName"]))->toBase64()->encoded : Avatar::create(ucfirst($this["userProfile"]["company_name"] ))->toBase64()->encoded; 

        }else
        {
            return $this->userProfile["image_path"];
        }
    }

    public function getAllUsers()
    {


        $users = $this->with("userProfile","userAccountTitle")->where("email", "<>", "")->get();
        foreach ($users as $key => $user) {
            $user->user_type_name = $user->userType == 1 ? "Candidate" : "Job Provider";
            $user->name = $user->userType == 1 ? $user["userProfile"]["firstName"]. " " .$user["userProfile"]["lastName"] : $user["userProfile"]["company_name"];
            $user->time_ago = $this->getTimeAgo($user->created_at);

            if($user["userProfile"]["image_path"] == "")
            {
                $user->image_path = Avatar::create( $user->userType == 1 ? ucfirst($user["userProfile"]["firstName"]) . " " . ucfirst($user["userProfile"]["lastName"]) : $user["userProfile"]["company_name"] )->toBase64()->encoded; 
            }


        }
        return $users;
    }
    public function contacts()
    {
        return $this->hasOne('App\UserContactList','user_contact_id');
    }

    public function jobs()
    {
        return $this->hasMany('App\Jobs', 'userID');
    }

    public function plans()
    {
        return $this->hasMany('App\PlanUser', 'user_id');
    }    

    public function canImpersonate()
    {
        // For example
        return $this->userAdmin == 1;
    }

    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    
}
