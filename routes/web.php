<?php

    
Route::group(['middleware' => 'web'], function()

{
	
    Route::get('/authentication/{provider}/redirect', 'AuthenticationController@redirectToProvider');
    Route::get('/authentication/{provider}/callback', 'AuthenticationController@handleProviderCallback');
    
});


