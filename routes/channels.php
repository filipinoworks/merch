<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
/*
Broadcast::channel('chat', function ($user) {
  return Auth::check();
});
*/
Broadcast::channel('chatroom.{convo_id}', function ($user) {
    return true;
});
Broadcast::channel('newMessage.{user_id}', function ($user) {
    return true;
});
Broadcast::channel('notifications.{user_id}', function ($user) {
    return true;
});
Broadcast::channel('onlineStatus.{user_id}', function () {
    return true;
});

Broadcast::channel('ethesiaEvents', function ($user) {
    return true;
});