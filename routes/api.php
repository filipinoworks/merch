<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::options('{any?}', function (){
    return response('',200);
})->where('any', '.*');

Route::group(['middleware' => 'cors','web'], function()

{

	Route::get('/prepare/roles', 'AdminController@prepareRoles');
	Auth::routes();    
	Route::post('/users/login_as', 'AdminController@impersonate');
	Route::post('/authentication/register', 'UsersController@register');

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/token', 'TokenController@token');
	Route::get('/user/check/login', 'UsersController@checkLogin');
	Route::get('/logmeout', 'UsersController@logmeout');

	/*products*/
	Route::post('/products', 'ProductController@store');
	Route::get('/products', 'ProductController@index');
	Route::get('/products/{id}', 'ProductController@show');
	Route::delete('/products/{id}', 'ProductController@delete');

	/*sales channel*/
	Route::post('/sales_channel', 'SalesChannelController@store');
	Route::get('/sales_channel', 'SalesChannelController@index');
	Route::get('/sales_channel/{id}/edit', 'SalesChannelController@show');
	Route::delete('/sales_channel/{id}/remove', 'SalesChannelController@delete');

	/*sales channel product*/
	Route::get('/sales_channel/product', 'SalesChannelProductController@index');
	Route::post('/sales_channel/product', 'SalesChannelProductController@store');
	Route::delete('/sales_channel/remove/{id}/product/', 'SalesChannelProductController@delete');
	Route::get('/sales_channel/edit/{id}/product', 'SalesChannelProductController@show');

	/*listing*/
	Route::post('/listing', 'ListingController@store');
	Route::get('/listing', 'ListingController@index');
	Route::delete('/listing/remove/{id}', 'ListingController@delete');
	Route::post('/upload/listing', 'ListingController@uploadPhotos');
	Route::get('/listing/images/{id}', 'ListingController@getPhoto');
	Route::get('/listing/get-images-by-listing/{id}', 'ListingController@photos');
	Route::delete('/listing/remove/images/{id}', 'ListingController@removePhoto');
	
	/*statuses*/
	Route::get('/status/{id}', 'ListingController@getStatusListing');


	/*research module*/
	Route::post('/research', 'ResearchController@store');
	Route::get('/research/new', 'ResearchController@getNewResearch');
	Route::get('/research/finished', 'ResearchController@getFinishedResearch');
	Route::get('/research/{id}/retrieve', 'ResearchController@show');
	Route::delete('/research/{id}/remove', 'ResearchController@delete');
	Route::post('/research/update-status', 'ResearchController@updateStatus');
	Route::get('/research/{id}/create-listing', 'ResearchController@createListing');

	/*team*/
	Route::post('/team', 'TeamController@store');
	Route::get('/team-members', 'TeamController@index');
	Route::get('/team/user/{user_id}/retrieve', 'TeamController@getUserById');

	/*plans*/
	Route::get('/plans', 'PlanController@index');
	Route::post('/plans/select-plan', 'PlanController@activatePlan');

	
});