-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2018 at 01:59 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `merchalong2`
--

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE `listings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp_one` text COLLATE utf8mb4_unicode_ci,
  `bp_two` text COLLATE utf8mb4_unicode_ci,
  `is_bind` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sales_channel_id` int(11) NOT NULL DEFAULT '0',
  `path_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `user_id`, `brand`, `title`, `keywords`, `bp_one`, `bp_two`, `is_bind`, `description`, `deleted_at`, `created_at`, `updated_at`, `sales_channel_id`, `path_code`) VALUES
(5, 1, 'ssss', 'dddd', NULL, NULL, NULL, 1, NULL, '2018-12-21 10:09:23', '2018-12-21 09:53:26', '2018-12-21 10:09:23', 0, '7i4pslIfzY'),
(6, 1, 'sdsd', 'dsdsd', NULL, NULL, NULL, 1, NULL, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04', 0, 'baz9DJZquR'),
(7, 1, 'Brand W', 'Tshirst', NULL, NULL, NULL, 1, NULL, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54', 0, 'TLNP2BAdsS');

-- --------------------------------------------------------

--
-- Table structure for table `listing_channels`
--

CREATE TABLE `listing_channels` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp_one` text COLLATE utf8mb4_unicode_ci,
  `bp_two` text COLLATE utf8mb4_unicode_ci,
  `is_bind` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `listing_photo_paths`
--

CREATE TABLE `listing_photo_paths` (
  `id` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` text COLLATE utf8mb4_unicode_ci,
  `filename` text COLLATE utf8mb4_unicode_ci,
  `height` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listing_photo_paths`
--

INSERT INTO `listing_photo_paths` (`id`, `path`, `path_code`, `width`, `filename`, `height`, `deleted_at`, `created_at`, `updated_at`) VALUES
(27, 'uploads/ayvgBw7k7Y/logo.png', 'ayvgBw7k7Y', '2048', 'logo.png', '1365', NULL, '2018-12-21 09:53:22', '2018-12-21 09:53:22'),
(28, 'uploads/undefined/login-register.jpg', 'undefined', '2500', 'login-register.jpg', '1668', NULL, '2018-12-21 09:57:39', '2018-12-21 09:57:39'),
(29, 'uploads/undefined/1-login-register.jpg', 'undefined', '2500', '1-login-register.jpg', '1668', NULL, '2018-12-21 09:59:55', '2018-12-21 09:59:55'),
(30, 'uploads/undefined/sidebar-4.jpg', 'undefined', '400', 'sidebar-4.jpg', '800', NULL, '2018-12-21 10:01:38', '2018-12-21 10:01:38'),
(31, 'uploads/undefined/sidebar-2.jpg', 'undefined', '400', 'sidebar-2.jpg', '800', NULL, '2018-12-21 10:03:16', '2018-12-21 10:03:16'),
(32, 'uploads/undefined/new_logo.png', 'undefined', '80', 'new_logo.png', '80', NULL, '2018-12-21 10:09:28', '2018-12-21 10:09:28'),
(90, 'uploads/TLNP2BAdsS/1-logo.png', 'TLNP2BAdsS', '2048', '1-logo.png', '1365', NULL, '2018-12-23 03:32:54', '2018-12-23 03:32:54'),
(91, 'uploads/TLNP2BAdsS/1-sidebar-2.jpg', 'TLNP2BAdsS', '400', '1-sidebar-2.jpg', '800', NULL, '2018-12-23 03:33:42', '2018-12-23 03:33:42'),
(93, 'uploads/TLNP2BAdsS/fanny counter.jpg', 'TLNP2BAdsS', '512', 'fanny counter.jpg', '287', NULL, '2018-12-24 20:07:49', '2018-12-24 20:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `listing_products`
--

CREATE TABLE `listing_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `listing_id` int(11) NOT NULL,
  `sales_channel_id` int(11) NOT NULL,
  `listing_channel` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `light_colors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_colors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp_one` text COLLATE utf8mb4_unicode_ci,
  `bp_two` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_light` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listing_products`
--

INSERT INTO `listing_products` (`id`, `listing_id`, `sales_channel_id`, `listing_channel`, `price`, `light_colors`, `dark_colors`, `product_name`, `title`, `bp_one`, `bp_two`, `description`, `brand`, `is_light`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(2, 1, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(3, 1, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(4, 1, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(5, 1, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(6, 1, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(7, 1, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'Tshirt', NULL, NULL, NULL, 'Nike', 0, NULL, '2018-12-21 06:06:14', '2018-12-21 06:06:14'),
(8, 2, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(9, 2, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(10, 2, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(11, 2, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(12, 2, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(13, 2, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:05', '2018-12-21 09:47:05'),
(14, 2, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'Tshirt', NULL, NULL, NULL, 'Brand x', 0, NULL, '2018-12-21 09:47:06', '2018-12-21 09:47:06'),
(15, 3, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(16, 3, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(17, 3, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(18, 3, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(19, 3, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(20, 3, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(21, 3, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'title', NULL, NULL, NULL, 'brand y', 0, NULL, '2018-12-21 09:48:50', '2018-12-21 09:48:50'),
(22, 4, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:42', '2018-12-21 09:51:42'),
(23, 4, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:42', '2018-12-21 09:51:42'),
(24, 4, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:43', '2018-12-21 09:51:43'),
(25, 4, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:43', '2018-12-21 09:51:43'),
(26, 4, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:43', '2018-12-21 09:51:43'),
(27, 4, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:43', '2018-12-21 09:51:43'),
(28, 4, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'dsdsd', NULL, NULL, NULL, 'sss', 0, NULL, '2018-12-21 09:51:43', '2018-12-21 09:51:43'),
(29, 5, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(30, 5, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(31, 5, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(32, 5, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(33, 5, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(34, 5, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(35, 5, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'dddd', NULL, NULL, NULL, 'ssss', 0, NULL, '2018-12-21 09:53:27', '2018-12-21 09:53:27'),
(36, 6, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(37, 6, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(38, 6, 4, 0, '400.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-24 20:38:35'),
(39, 6, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(40, 6, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(41, 6, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(42, 6, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'dsdsd', NULL, NULL, NULL, 'sdsd', 0, NULL, '2018-12-21 10:10:04', '2018-12-21 10:10:04'),
(43, 7, 4, 0, '100.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-24 20:38:16'),
(44, 7, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54'),
(45, 7, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54'),
(46, 7, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54'),
(47, 7, 4, 0, '0.00', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54'),
(48, 7, 5, 0, '0.00', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54'),
(49, 7, 6, 0, '0.00', 'null', 'null', 'Mug, Black 11 oz', 'Tshirst', NULL, NULL, NULL, 'Brand W', 0, NULL, '2018-12-21 13:15:54', '2018-12-21 13:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_12_20_112635_create_listing_channels_table', 1),
(2, '2018_12_20_112635_create_listing_products_table', 1),
(3, '2018_12_20_112635_create_listings_table', 1),
(4, '2018_12_20_112635_create_products_table', 1),
(5, '2018_12_20_112635_create_sales_channel_products_table', 1),
(6, '2018_12_20_112635_create_sales_channel_table', 1),
(7, '2018_12_20_112635_create_sessions_table', 1),
(8, '2018_12_20_112635_create_users_profile_table', 1),
(9, '2018_12_20_112635_create_users_table', 1),
(11, '2018_12_21_121041_listing_photo_paths', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bp_one` text COLLATE utf8mb4_unicode_ci,
  `bp_two` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_channel`
--

CREATE TABLE `sales_channel` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `channel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supports_upc_sku` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_channel`
--

INSERT INTO `sales_channel` (`id`, `user_id`, `channel_name`, `supports_upc_sku`, `default`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'Merch by Amazon', 0, 1, NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22'),
(2, 0, 'Teepublic', 0, 1, NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22'),
(3, 0, 'Etsy', 0, 1, NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22'),
(4, 1, 'Merch by Amazon', 0, 1, NULL, '2018-12-20 03:27:53', '2018-12-20 03:27:53'),
(5, 1, 'Teepublic', 0, 1, NULL, '2018-12-20 03:27:54', '2018-12-20 03:27:54'),
(6, 1, 'Etsy', 0, 1, NULL, '2018-12-20 03:27:54', '2018-12-20 03:27:54');

-- --------------------------------------------------------

--
-- Table structure for table `sales_channel_products`
--

CREATE TABLE `sales_channel_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `sales_channel_id` int(11) NOT NULL,
  `max_colors` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `light_colors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_colors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bp_one` text COLLATE utf8mb4_unicode_ci,
  `bp_two` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `default_listing` tinyint(1) NOT NULL DEFAULT '0',
  `src_product_id` int(11) DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_light` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_channel_products`
--

INSERT INTO `sales_channel_products` (`id`, `sales_channel_id`, `max_colors`, `price`, `deleted_at`, `created_at`, `updated_at`, `light_colors`, `dark_colors`, `product_name`, `title`, `bp_one`, `bp_two`, `description`, `default_listing`, `src_product_id`, `brand`, `is_light`) VALUES
(1, 1, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', '', '', '', '', 0, NULL, NULL, 0),
(2, 1, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', '', '', '', '', 0, NULL, NULL, 0),
(3, 1, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', '', '', '', '', 0, NULL, NULL, 0),
(4, 1, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', '', '', '', '', 0, NULL, NULL, 0),
(5, 1, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', '', '', '', '', 0, NULL, NULL, 0),
(6, 2, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', '', '', '', '', 0, NULL, NULL, 0),
(7, 3, 0, '0.00', NULL, '2018-12-20 03:27:22', '2018-12-20 03:27:22', '', '', 'Mug, Black 11 oz', '', '', '', '', 0, NULL, NULL, 0),
(8, 4, 0, '300.00', NULL, '2018-12-20 03:27:53', '2018-12-24 20:42:03', '[\"#ffffff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Long Sleeve Shirt', '', NULL, NULL, NULL, 0, 1, NULL, 0),
(9, 4, 0, '0.00', NULL, '2018-12-20 03:27:53', '2018-12-20 03:27:53', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'T-Shirt', '', '', '', '', 0, 2, NULL, 0),
(10, 4, 0, '0.00', NULL, '2018-12-20 03:27:53', '2018-12-20 03:27:53', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Hoodie', '', '', '', '', 0, 3, NULL, 0),
(11, 4, 0, '0.00', NULL, '2018-12-20 03:27:53', '2018-12-20 03:27:53', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Sweatshirt', '', '', '', '', 0, 4, NULL, 0),
(12, 4, 0, '0.00', NULL, '2018-12-20 03:27:54', '2018-12-20 03:27:54', '[\"#fff\"]', '[\"#4f5555\",\"#d7dbdc\",\"#000000\",\"#15232b\",\"#1c4086\"]', 'Premium T-Shirt', '', '', '', '', 0, 5, NULL, 0),
(13, 5, 0, '0.00', NULL, '2018-12-20 03:27:54', '2018-12-20 03:27:54', '[\"#FFFFFF\"]', '[\"#000000\"]', 'Teepublic Products', '', '', '', '', 0, 6, NULL, 0),
(14, 6, 0, '0.00', NULL, '2018-12-20 03:27:54', '2018-12-20 03:27:54', 'null', 'null', 'Mug, Black 11 oz', '', '', '', '', 0, 7, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userType` tinyint(1) NOT NULL,
  `userTitle` tinyint(1) DEFAULT NULL,
  `userAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `is_confirmed` enum('yes','no') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`, `email`, `image_path`, `stripe_id`, `remember_token`, `userType`, `userTitle`, `userAdmin`, `is_confirmed`) VALUES
(1, 'admin@merchalong.com', '$2y$10$1HjXmTaxyc8xvoo7Nq8WdeeoQrauqxVHLC6M/HNruhOVt3dJPZYJe', '2018-12-20 03:27:23', '2018-12-20 03:27:23', 'admin@merchalong.com', NULL, NULL, NULL, 0, NULL, 0, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

CREATE TABLE `users_profile` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `firstName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimumSalary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `allowSearch` enum('yes','no') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailPreference` enum('html','plain') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialPromotions` tinyint(1) DEFAULT NULL,
  `defaultPayment` tinyint(1) DEFAULT NULL COMMENT '1-CreditCard,2-eCheck',
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebookUrl` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtubeUrl` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitterUrl` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentSalary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expectedSalary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `positionDuration` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_education` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_education` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_education` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resume_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coverLetter` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `linkedinUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `googlePlusUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_line` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`id`, `userID`, `firstName`, `lastName`, `address`, `city`, `state`, `country`, `zipcode`, `phone`, `fax`, `minimumSalary`, `description`, `allowSearch`, `website`, `company_name`, `emailPreference`, `specialPromotions`, `defaultPayment`, `image_path`, `facebookUrl`, `youtubeUrl`, `twitterUrl`, `currentSalary`, `expectedSalary`, `positionDuration`, `title_education`, `from_education`, `to_education`, `school_name`, `resume_path`, `coverLetter`, `created_at`, `updated_at`, `linkedinUrl`, `googlePlusUrl`, `tag_line`, `name`) VALUES
(1, 1, 'Admin', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-20 03:27:23', '2018-12-20 03:27:23', '', '', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `listings`
--
ALTER TABLE `listings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listing_channels`
--
ALTER TABLE `listing_channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listing_photo_paths`
--
ALTER TABLE `listing_photo_paths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listing_products`
--
ALTER TABLE `listing_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_channel`
--
ALTER TABLE `sales_channel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_channel_products`
--
ALTER TABLE `sales_channel_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Index` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `listings`
--
ALTER TABLE `listings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `listing_channels`
--
ALTER TABLE `listing_channels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listing_photo_paths`
--
ALTER TABLE `listing_photo_paths`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `listing_products`
--
ALTER TABLE `listing_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_channel`
--
ALTER TABLE `sales_channel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sales_channel_products`
--
ALTER TABLE `sales_channel_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_profile`
--
ALTER TABLE `users_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
