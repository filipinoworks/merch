<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Research extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('researchs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('status')->default("new");
            $table->string('path_code')->nullable();
            $table->string('design_phrase')->nullable();
            $table->string('link')->nullable();
            $table->string('keys')->nullable();
            $table->string('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('researchs');
    }
}
