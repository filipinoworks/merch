<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListingPhotoPaths extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('listing_photo_paths', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('path')->nullable();
            $table->string('path_code')->nullable();
            $table->text("width")->nullable();
            $table->text("filename")->nullable();
            $table->text('height')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_photo_paths');
    }
}
