<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('username', 100);
			$table->string('password');
			$table->timestamps();
			$table->string('email', 100);
			$table->string('image_path')->nullable();
			$table->string('stripe_id')->nullable();
			$table->string('remember_token')->nullable();
			$table->boolean('userType');
			$table->boolean('userTitle')->nullable();
			$table->boolean('userAdmin')->default(0);
			$table->enum('is_confirmed', array('yes','no'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
