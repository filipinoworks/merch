<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListingChannelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listing_channels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('brand')->nullable();
			$table->string('keywords')->nullable();
			$table->text('bp_one', 65535)->nullable();
			$table->text('bp_two', 65535)->nullable();
			$table->boolean('is_bind')->default(0);
			$table->text('description', 65535)->nullable();
			$table->string('title')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listing_channels');
	}

}
