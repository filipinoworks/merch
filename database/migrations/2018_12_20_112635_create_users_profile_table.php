<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_profile', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('userID')->index('Index');
			$table->string('firstName', 40)->nullable();
			$table->string('lastName', 60)->nullable();
			$table->string('address')->nullable();
			$table->string('city', 100)->nullable();
			$table->string('state', 60)->nullable();
			$table->string('country', 60)->nullable();
			$table->string('zipcode', 15)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('fax', 20)->nullable();
			$table->string('minimumSalary', 100)->nullable();
			$table->text('description', 65535)->nullable();
			$table->enum('allowSearch', array('yes','no'))->nullable();
			$table->string('website')->nullable();
			$table->string('company_name', 100)->nullable();
			$table->enum('emailPreference', array('html','plain'))->nullable();
			$table->boolean('specialPromotions')->nullable();
			$table->boolean('defaultPayment')->nullable()->comment('1-CreditCard,2-eCheck');
			$table->string('image_path')->nullable();
			$table->string('facebookUrl', 100)->nullable();
			$table->string('youtubeUrl', 100)->nullable();
			$table->string('twitterUrl', 100)->nullable();
			$table->string('currentSalary', 100)->nullable();
			$table->string('expectedSalary', 100)->nullable();
			$table->string('positionDuration', 100)->nullable();
			$table->string('title_education', 100)->nullable();
			$table->string('from_education', 100)->nullable();
			$table->string('to_education', 100)->nullable();
			$table->string('school_name', 100)->nullable();
			$table->string('resume_path')->nullable();
			$table->string('coverLetter', 5000)->nullable();
			$table->timestamps();
			$table->string('linkedinUrl');
			$table->string('googlePlusUrl');
			$table->string('tag_line');
			$table->string('name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_profile');
	}

}
