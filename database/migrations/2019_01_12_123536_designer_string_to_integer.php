<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DesignerStringToInteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        

        if(Schema::hasColumn('listings' , "designer")) 
        {
            Schema::table('listings', function(Blueprint $table)
            {
                $table->dropColumn('designer');
            });


        } 

        Schema::table('listings', function(Blueprint $table)
        {
            $table->integer('designer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('listings', function(Blueprint $table)
        {
            $table->dropColumn('designer');
        });

    }
}
