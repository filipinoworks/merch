<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListingProductsDefaultColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {      
        if (!Schema::hasColumn('listing_products', 'default_colors'))
        {
            Schema::table('listing_products', function(Blueprint $table)
            {
                $table->string('default_colors')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('listing_products', function(Blueprint $table)
        {
            $table->dropColumn('default_colors');
        });
    }
}
