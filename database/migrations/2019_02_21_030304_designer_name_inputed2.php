<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DesignerNameInputed2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function(Blueprint $table)
        {
            $table->string('input_designer')->nullable();
            $table->string('unregistered_designer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('listings', function(Blueprint $table)
        {
            $table->dropColumn('input_designer');
            $table->dropColumn('unregistered_designer');
        });
    }
}
