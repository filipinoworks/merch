<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListingProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listing_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('listing_id');
			$table->integer('sales_channel_id');
			$table->integer('listing_channel');
			$table->decimal('price', 10)->nullable();
			$table->string('light_colors')->nullable();
			$table->string('dark_colors')->nullable();
			$table->string('product_name')->nullable();
			$table->string('title')->nullable();
			$table->text('bp_one', 65535)->nullable();
			$table->text('bp_two', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('brand')->nullable();
			$table->boolean('is_light')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listing_products');
	}

}
