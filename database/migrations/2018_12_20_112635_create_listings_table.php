<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('brand')->nullable();
			$table->string('title')->nullable();
			$table->string('keywords')->nullable();
			$table->text('bp_one', 65535)->nullable();
			$table->text('bp_two', 65535)->nullable();
			$table->boolean('is_bind')->default(0);
			$table->text('description', 65535)->nullable();
			$table->softDeletes();
			$table->timestamps();
			$table->integer('sales_channel_id')->default(0);
			$table->string('path_code')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listings');
	}

}
