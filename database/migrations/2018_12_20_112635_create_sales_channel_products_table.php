<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesChannelProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_channel_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sales_channel_id');
			$table->integer('max_colors')->default(0);
			$table->decimal('price', 10)->nullable();
			$table->softDeletes();
			$table->timestamps();
			$table->string('light_colors')->nullable();
			$table->string('dark_colors')->nullable();
			$table->string('product_name');
			$table->string('title');
			$table->text('bp_one', 65535)->nullable();
			$table->text('bp_two', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->boolean('default_listing')->default(0);
			$table->integer('src_product_id')->nullable();
			$table->string('brand')->nullable();
			$table->boolean('is_light')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_channel_products');
	}

}
