<?php

use Illuminate\Database\Seeder;
use App\SalesChannel;
use App\SalesChannelProduct;

class DefaultSalesChannels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//SalesChannel::query()->truncate();
    	//SalesChannelProduct::query()->truncate();

        //Merch by Amazon
        $channel_name = "Merch by Amazon";
        $products = ["Standard T-Shirt" , "Premium T-Shirt" , "Long Sleeve Shirt", "Sweatshirt",  "Pullover Hoodie"];
        
    	$sales_chan_ins = new SalesChannel();

        if(count($sales_chan_ins->all()) > 0) {
            return;
        }

    	$sales_chan_ins->user_id = "";
    	$sales_chan_ins->channel_name = $channel_name;
    	$sales_chan_ins->supports_upc_sku = false;
    	$sales_chan_ins->default = true;
    	$sales_chan_ins->save();

	    foreach ($products as $key => $product) {	

	    	$ins = new SalesChannelProduct();

            if($product == "Standard T-Shirt" || $product == "Premium T-Shirt")
            {
                $dark_colors  = [];
                
                $light_colors = [];

                $default_colors = ["#818189", "#84afd6" , "#FFFFFF", "#f0e87b", "#6e0a25", "#f8a3bc", "#ff5c39", "#514689", "#5e9444" , "#4f5555", "#d7dbdc", "#000000", "#15232b", "#1c4086", "#61261d", "#b71111", "#3f3e3c" , "#4a4f26", "#006136"];

                $max_colors = "5";

            }else if($product == "Pullover Hoodie" || $product == "Long Sleeve Shirt" || $product == "Sweatshirt")
            {
                $dark_colors    = [];
                
                $light_colors   = [];
                
                $default_colors = ["#4f5555", "#d7dbdc", "#000000", "#15232b", "#1c4086"];
                
                $max_colors     = "5";
            }else
            {
                $dark_colors = [];

                $light_colors = [];

                $default_colors = [];

                $max_colors = "0";
            }

	    	$ins->light_colors = json_encode($light_colors);
	    	$ins->dark_colors = json_encode($dark_colors);
            $ins->default_colors = json_encode($default_colors);

	    	$ins->sales_channel_id = $sales_chan_ins->id;
	    	$ins->price = 0;
            $ins->default_listing = 1;
	    	//$ins->product_id = $data["product_id"];
	        $ins->max_colors = $max_colors;
	        $ins->product_name = $product;
	        $ins->title = "";
	        $ins->bp_one = "";
	        $ins->bp_two = "";
	        $ins->description = "";
	    	$ins->save();
        }

       

        //Teepublic
        $channel_name = "Teepublic";
        $products = ["Teepublic Products"];
        
    	$sales_chan_ins = new SalesChannel();
    	$sales_chan_ins->user_id = "";
    	$sales_chan_ins->channel_name = $channel_name;
    	$sales_chan_ins->supports_upc_sku = false;
    	$sales_chan_ins->default = true;
    	$sales_chan_ins->save();

    	$dark_colors = ["#000000"];
    	$light_colors = ["#FFFFFF"];

	    foreach ($products as $key => $product) {	

	    	$ins = new SalesChannelProduct();
	    	$ins->light_colors = json_encode($light_colors);
	    	$ins->dark_colors = json_encode($dark_colors);
            $ins->default_colors = json_encode($default_colors);
	    	$ins->sales_channel_id = $sales_chan_ins->id;
            $ins->max_colors = 0;
            $ins->default_listing = 1;
	    	$ins->price = 0;
	    	//$ins->product_id = $data["product_id"];
	        //$ins->max_colors = $data["max_colors"];
	        $ins->product_name = $product;
	        $ins->title = "";
	        $ins->bp_one = "";
	        $ins->bp_two = "";
	        $ins->description = "";
	    	$ins->save();
        }



        //Etsy
        $channel_name = "Spreadshirt";
        $products = ["Spreadshirt Products"];
        
    	$sales_chan_ins = new SalesChannel();
    	$sales_chan_ins->user_id = "";
    	$sales_chan_ins->channel_name = $channel_name;
    	$sales_chan_ins->supports_upc_sku = false;
    	$sales_chan_ins->default = true;
    	$sales_chan_ins->save();

    	$dark_colors = ["#000000"];
        $light_colors = ["#FFFFFF"];

	    foreach ($products as $key => $product) {	

	    	$ins = new SalesChannelProduct();
	    	$ins->light_colors = json_encode($light_colors);
            $ins->dark_colors = json_encode($dark_colors);
            $ins->default_colors = json_encode($default_colors);
	    	$ins->sales_channel_id = $sales_chan_ins->id;
            $ins->max_colors = 0;
            $ins->default_listing = 1;
	    	$ins->price = 0;
	    	//$ins->product_id = $data["product_id"];
	        //$ins->max_colors = $data["max_colors"];
	        $ins->product_name = $product;
	        $ins->title = "";
	        $ins->bp_one = "";
	        $ins->bp_two = "";
	        $ins->description = "";
	    	$ins->save();
        }

    }
}
