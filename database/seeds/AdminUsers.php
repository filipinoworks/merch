<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UsersProfile;


class AdminUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\User::query()->truncate();
        \App\UsersProfile::query()->truncate();

        $data = new User();
        $data->email = "admin@merchalong.com";
        $data->password = Hash::make("looping123");
        $data->username ="admin@merchalong.com";
        $data->is_confirmed = "yes";
        $data->save();

        $data->assignRole("Owner");

        $users_profile = new UsersProfile();
        $users_profile->firstName = "Admin";
        $users_profile->lastName = "";
        $users_profile->userID = $data->id;
        $users_profile->save();
    }
}
