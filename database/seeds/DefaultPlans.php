<?php

use Illuminate\Database\Seeder;
use App\Plan;

class DefaultPlans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	Plan::query()->truncate();

        $plan               = new Plan;
        $plan->title        = "Extended 1,000";
        $plan->description  = "1,000 Listings";
        $plan->price        = "30.00";
        $plan->monthly_type = "monthly"; 
        $plan->save();  
        
        $plan               = new Plan;
        $plan->title        = "Extended 2,500";
        $plan->description  = "2,500 Listings";
        $plan->price        = "40.00"; 
        $plan->monthly_type = "monthly";
        $plan->save();
        
        $plan               = new Plan;
        $plan->title        = "Extended 5,000";
        $plan->description  = "5,000 Listings";
        $plan->price        = "55.00"; 
        $plan->monthly_type = "monthly";
        $plan->save();
        
        $plan               = new Plan;
        $plan->title        = "Extended 7,500";
        $plan->description  = "7,500 Listings";
        $plan->price        = "70.00"; 
        $plan->monthly_type = "monthly";
        $plan->save(); 
        
        $plan               = new Plan;
        $plan->title        = "Extended 10,000";
        $plan->description  = "10,000 Listings";
        $plan->price        = "80.00"; 
        $plan->monthly_type = "monthly";
        $plan->save(); 
    }


}